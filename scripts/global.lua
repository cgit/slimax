-- SLIMax Mgr Lua Script v2.4
-- PART OF SLIMAX Manager pkg
-- Copyright (c)2011-2013 by EK and Zappadoc - All Rights Reserved.
-- changed by Zappadoc - 2012-11-08

-- see slimax_script_readme.txt for implementation

-- ============================================================
-- GLOBAL VAR
-- ============================================================
-- scripts settings
mCustomScriptsFileName = ""

-- new in: 2.2
-- init left and right digits panel text	
mLeftSpdLmtText = "      "
mRightSpdLmtText = "      "

-- new: 2.1
-- RPM threshold values in percentage used with Shiftlights method 2
-- and configured with SLIMax Manager II Advanced Options Panel
-- default values:
RPM_PERCENT_VALUES = {50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 97, 98, 99 }
RPM_ABSOLUTE_VALUES = {15452, 15545, 15823, 16354, 16410, 16675, 17252, 17545, 17823, 18354, 18510, 18655, 18675 }

-- true if you want to display switch pos and value
mSwitchFeedbackAllowed = false 

-- mapping to keystroke demo script
-- see the support forum for more info on key mapping 
mDemoMapToKeyAllowed = false
mDemoMapToKeySwitch = 5

-- Low Fuel status
mLowFuelFeedbackAllowed = true

-- Left and Right Digits status
mLeftDigitsFeedbackAllowed = false
mRightDigitsFeedbackAllowed = false

-- display KPH or KMH
mUnitFeedbackAllowed = true

-- show the current OSP Factor
mOSPFeedbackAllowed = true

-- cool global brightness feedback 
mBrightnessFeedbackAllowed = true

-- show the current value of maxgear option
mMaxGearFeedbackAllowed = true

-- activate or deactivate lap dump telemetry
-- using button 9 by default
mDumpLapAllowed = false
mDumpLapButton = 9

-- delta time delay
mDeltaTimeBackup = 0
mDeltaTimeOldTicks = 0
mDeltaTimeDelay = 700

-- ============================================================

-- init globals flag
isGlobalInitialized = 0

-- 13 RPM leds
mRPMLedTable = { 
    RPM0=0, 
    RPM1=0, 
    RPM2=0,
    RPM3=0,
    RPM4=0, 
    RPM5=0,
    RPM6=0,
    RPM7=0,
    RPM8=0,
    RPM9=0,
    RPM10=0,
    RPM11=0,
    RPM12=0 
}


-- SLI device type
mDeviceType = { 
	"BU0386Otus",
	"BU0386",
	"BU0386a",
	"BU0386lc",
	"BU0386x",
	"SLI-M",
	"SLI-PRO",
	"SPARE1",
	"BU0710", 
	"SLIEMU", 
	"SLIF1"
}

-- ========================
-- Utilities functions
-- ======================== 

-- add a function name to left and right functions list
-- typical usage AddFuncName(path, func): 
-- on left list:
-- local err = AddFuncName("cfg/sli_left_functions.ecfg", "100.DSII KERS")
-- or right list:
-- local err = AddFuncName("cfg/sli_right_functions.ecfg", "100.DSII KERS")
function AddFuncName(path, func)
	file,err = io.open(path,"r")
	if err then return 0 end

	isOK = false
	t = {}
	i= 1
	for line in file:lines() do
		if line == nil then break end
		-- check if the function name exist
		 if string.find(line, func)then isOK = true end
		 t[i]= line
		 i= i + 1
	end
	file:close()

	if not isOK then
		-- write mode
		file, err = io.open(path,"w+")
		if err then return 0 end
		-- add the function name at the end of file
		for k, v in pairs(t) do
			-- add line
			file:write(v)
			file:write("\n")
		end
		-- add the function name
		file:write(func)
		file:write("\n")
		file:close()
	end
	return 1
end

 
 -- same as above for removing the function name
function RemoveFuncName(path, func)
   file,err = io.open(path,"r")
   if err then return 0 end
    
   t = { }
   i=1
   for line in file:lines() do
      if line == nil then break end
      -- check if the function name exist
      if not string.find(line, func)then
         t[i] = line
         i= i + 1
      end
   end
   file:close()
 
   -- write mode, delete previous data
   file, err = io.open(path,"w+")
   if err then return 0 end
    
   for k, v in pairs(t) do
    --  print(k .. " - " .. v .. "\n")
        -- add line
      file:write(v)
      file:write("\n")
   end
    
   file:close()
   return 1
end


-- get next function index available
-- typical usage GetNewFuncIndex(path): 
-- on left list:
-- local num_available = GetNewFuncIndex("cfg/sli_left_functions.ecfg")
-- or right list:
-- local num_available = GetNewFuncIndex("cfg/sli_right_functions.ecfg")
function GetNewFuncIndex(path)
   file,err = io.open(path,"r")
   if err then return 0 end
 
   idx = 0
   for line in file:lines() do
      if line == nil then break end
      -- get the prefix
      a =  string.find(line, '.', 1, true)
      b = string.sub( line,1, a-1)
      -- print ( b .. "\n")
       
      -- string to num conversion
      c = (b + 0)
      -- store if it's greater
      if c > idx then idx = c end
   end
   file:close()
 
   return idx+1
end

-- Init all globals
function InitGlobals()
	-- default blink time delay
	mBlinkTime = 32
	mOSPBlinkTime = 8
	
	-- ticks stuff
	mOSPBlink = 0
	mSpdLmtBlink = 0
	mOldTickCount = 0
	mOldOSPTickCount = 0
	
	gRedZone = 0
	gOSPLimit = 0
	mBU0710Leds = 0
end

-- set RPM threshold value in percentage ( SLIMax Manager 2.1 )
function SetRPMPercentValue(index, value)
	if index <1 or index>13 then
		return
	end
	if value <0 or value >100 then
		return
	end
	
	RPM_PERCENT_VALUES[index] = value
end

-- set RPM threshold value ( SLIMax Manager 2.1 )
function SetRPMAbsoluteValue(index, value)
	if index <1 or index>13 then
		return
	end
	if value <0 or value >20000 then
		return
	end
	
	RPM_ABSOLUTE_VALUES[index] = value
end

-- get all global preferences set in general_default.sli
function GetSLIMaxInfo()
	-- get RPM Only flag
	mSpdLmtRPMLedOnly = GetContextInfo("spdlmtrpmledonlyflag")
	if mSpdLmtRPMLedOnly == nil then mSpdLmtRPMLedOnly = false end
	
	-- get blink delay value*
	mBlinkTime = GetContextInfo("blinktime")
	if mBlinkTime == nil then mBlinkTime = 32 end
	if mBlinkTime < 1 or  mBlinkTime > 48 then mBlinkTime = 32 end
	
	-- get osp blink delay value
	mOSPBlinkTime = GetContextInfo("ospblinktime")
	if mOSPBlinkTime == nil then mOSPBlinkTime = 32 end
	if mOSPBlinkTime < 1 or  mOSPBlinkTime > 48 then mOSPBlinkTime = 32 end

	-- get current speed Limiter LED index
	mSpeedLimiterLED = GetContextInfo("ledspeedlimiterindex")
	if mSpeedLimiterLED == nil then mSpeedLimiterLED = 5 end
	if mSpeedLimiterLED < 1 or  mSpeedLimiterLED > 6 then mSpeedLimiterLED = 5 end
	
	-- get current osp LEDs index
	mOSPLED1 = GetContextInfo("ospled1")
	if mOSPLED1 == nil then mOSPLED1 = 1 end
	if mOSPLED1 < 1 or  mOSPLED1 > 11 then mOSPLED1 = 1 end
	
	mOSPLED2 = GetContextInfo("ospled2")
	if mOSPLED2 == nil then mOSPLED2 = 1 end
	if mOSPLED2 < 1 or  mOSPLED2 > 11 then mOSPLED2 = 1 end

	-- blinking allowed?
	mNoBlink = GetContextInfo("noblinkflag")
	if mNoBlink == nil then mNoBlink = false end

	-- get limiter char flag
	mLimiterChar = GetContextInfo("limitercharflag")
	if mLimiterChar == nil then mLimiterChar = false end

	-- is OSP with first gear allowed?
	mOSPWithFirstGear = GetContextInfo("ospwithfirstgear")
	if mOSPWithFirstGear == nil then mOSPWithFirstGear = false end
	
end

-- get cpu ticks
 function GetTicks()
	local tcks = GetContextInfo("ticks")
	if tcks == nil then tcks  = 0 end
	return tcks
end

-- reset table function
function initLedTable(ibl, value)
	if ibl ~= nil then
		for k, v in pairs(ibl) do
			ibl[k] = value
		end
	end
end 

-- toggle all led state
function toggleAllLed(val)
	initLedTable(mRPMLedTable, val)
	for i = 1, 6 do SetWarnLed(i, val) end
	for i = 1, 5 do SetExtLed(i, val) end
	SetRPMLed("mRPMLedTable")
end

-- The following function rounds a number
-- lua doesn't return the same rounded value as C/C++
function round(num) 
    if num == nil then num = 0 end
    local i = 0
    if num >= 0 then 
        i = math.floor(num+.5) 
    else 
        i = math.ceil(num-.5) 
    end
    return i
end

-- The following function calculate hr, mn, sec, hd, ms
-- param: time in meter/sec
function timeDispatcher( tt)
    if tms == nil then tms = 0 end
	local tms = math.abs(tt)
	local t_hr = 0
	local t_mn = 0
	local t_sc = 0
	local t_ms = 0
	local t_hd = 0

	if tms > 0 then
	    t_hr, n = math.modf(tms/3600)
        t_mn, c = math.modf(n*60)
        t_sc, s = math.modf(c*60)
        t_hd, h = math.modf(s*100)
        t_ms, m = math.modf(s*1000)
	end
--		print( tt, t_hr, t_mn, t_sc, t_hd, t_ms)
	return t_hr, t_mn, t_sc, t_hd, t_ms
end

-- return speed in KPH or MPH
function speed(spd, selector)
    if spd == nil then spd = 0 end
	if selector then
		-- MPH
		return ( spd * 2.237)
	else
		return (spd * 3.6)
	end
end

-- Fahrenheit to Celsius
function FtoC(f)
	return ((f - 32) * (5/9))
end

-- Celsius to Fahrenheit
function CtoF(c)
	return (c * (9/5) + 32)
end

-- Kelvin to Celsius
function KtoC(k)
	return ( k - 273.313)
end 

-- liters to gallons
function LtoG(liters)
 return (liters * 0.264172052 )
end

-- gallons to liters
function GtoL(gallons)
 return (gallons * 3.78541178 )
end

-- return fuel in liters or gallons
function GetFuel(fl, selector)
    if fl == nil then fl = 0 end
	if selector then
		-- gallons
		return LtoG( fl)
	else
		return fl
	end
end

-- return celcius or fahrenheit
function GetTemp(tmp, selector)
    if tmp == nil then tmp = 0 end
	if selector then
		-- gallons
		return CtoF( tmp)
	else
		return tmp
	end
end

-- set delta time delay in ms
function SetDeltaTimeDelay(delay)
    if delay == nil or delay < 0 or delay > 5000 then delay = 700 end
	mDeltatimeDelay = delay
end

-- get current gear (even when engine/ignition is off)
function GetCurrentGear()
   local g = GetCarInfo("gear")
   -- get neutral
   local n = GetContextInfo("neutral")
   -- get reverse char and convert to string
   local r = string.char(GetContextInfo("reverse"))
   -- get state of custom Optimal Shift Point (OSP) records
   local ospcustom_on = GetContextInfo("ospcustom")

   -- set neutral, reverse or current gear
   local result = g

   if g == 0 then
      -- if neutral and using custom OSP record then add a dot to gear digit
      if ospcustom_on  then n = n + 128 end
      result = string.char(n)
   elseif g < 0 then
      -- reverse
      result = r
   end

   return result
end


--==============================================
require "scripts/slidevice"
