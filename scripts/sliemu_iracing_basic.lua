-- iRacing Custom Scripts - SLIMax Manager Scripts v1.8
-- Copyright �2011-2013 by Zappadoc - All Rights Reserved.
-- last change by Zappadoc - 2012-09-16

-- ================================
-- add the short desciption of your script(s) in mScript_Info global var
mScript_Info = "A useful iRacing custom scripts to select automatically the good settings for each iracing car you drive (shiftlights, max gears, osp, et.)."

-- ================================
-- load common scripts file
require "scripts/zdoc_scripts/iracing_common_scripts"