-- SLIMax Mgr Lua Script v2.1
-- Copyright (c)2011-2013 by EK and Zappadoc - All Rights Reserved.
-- Use this script to build all speed-limiter feedback methods
-- last change by Zappadoc - 2012-10-06

-- SLI-M, SLI-PRO speed limiter Methods
function spdLmtMethodEvent(idx)
	mSpdLimitMethod = idx
	mLeftSpdLmtText = "      "
	mRightSpdLmtText = "      "
	-- call custom script
	local result = custom_spdLmtMethodEvent(mSpdLimitMethod)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- call global custom script
	result = global_custom_spdLmtMethodEvent(mSpdLimitMethod)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	local led = ""
	
	-- check if initialized
	if isGlobalInitialized == 0 then
		isGlobalInitialized = 1
		-- init them
		InitGlobals()
	end
	-- get global prefs
	GetSLIMaxInfo()
	
	-- init table
	--initLedTable(mRPMLedTable, 0)
	--SetRPMLed("mRPMLedTable")
	
	if mNoBlink or (mSpdLimitMethod == 0) then
		-- speedlimiter led NOT blinking
		SetWarnLed(mSpeedLimiterLED, 1)
		
	elseif mSpdLimitMethod == 1 then
		-- speedlimiter led blinking, method 1
		if GetTicks() > mOldTickCount then
			mSpdLmtBlink = mSpdLmtBlink + 1
		end

		if mSpdLmtBlink >= mBlinkTime then
			mSpdLmtBlink = 0
		end

		if mSpdLmtBlink <= (mBlinkTime / 2) then
			if mLimiterChar then SetGearDigit("L") end
			SetWarnLed(mSpeedLimiterLED, 1)
		end

		if mSpdLmtBlink > (mBlinkTime / 2) then
			if mLimiterChar then SetGearDigit(" ") end
			SetWarnLed(mSpeedLimiterLED, 0)
		end
		
	elseif mSpdLimitMethod == 2 then
		-- speedlimiter led + RPM blinking, method 2
		if GetTicks() > mOldTickCount then
			mSpdLmtBlink = mSpdLmtBlink + 1
		end

		if mSpdLmtBlink >= mBlinkTime then
			mSpdLmtBlink = 0
		end
		
		if mSpdLmtBlink <= (mBlinkTime / 2) then
			if mLimiterChar then SetGearDigit("L") end
			initLedTable(mRPMLedTable, 1)
			if not mSpdLmtRPMLedOnly then 
				SetWarnLed(mSpeedLimiterLED, 1)
			end
		end

		if mSpdLmtBlink > (mBlinkTime / 2) then
			if mLimiterChar then SetGearDigit(" ") end
			initLedTable(mRPMLedTable, 0)
			if not mSpdLmtRPMLedOnly then 
				SetWarnLed(mSpeedLimiterLED, 0)
			end
		end
		SetRPMLed("mRPMLedTable")
		
	elseif mSpdLimitMethod == 3 then
		-- speedlimiter led + alternate RPM blinking, method 3
		if GetTicks() > mOldTickCount then
			mSpdLmtBlink = mSpdLmtBlink + 1
		end
		if mSpdLmtBlink >= mBlinkTime then
			mSpdLmtBlink = 0
		end
		if mSpdLmtBlink <= (mBlinkTime / 2) then
			if mLimiterChar then
				SetGearDigit("L") 
			end
			for i = 0,12 do 
				led = led.format("RPM%d",i)
				if (i % 2)==0 then
					mRPMLedTable[led] = 1
				else
					mRPMLedTable[led] = 0
				end
			end
			if not mSpdLmtRPMLedOnly then 
				SetWarnLed(mSpeedLimiterLED, 1)
			end			
		end

		if mSpdLmtBlink > (mBlinkTime / 2) then
			if mLimiterChar then 
				SetGearDigit(" ") 
			end
			for i = 0,13 do 
				led = led.format("RPM%d",i)
				if (i % 2)==0 then
					mRPMLedTable[led] = 0
				else
					mRPMLedTable[led] = 1
				end
			end
			if not mSpdLmtRPMLedOnly then 
				SetWarnLed(mSpeedLimiterLED, 0)
			end	
		end
		SetRPMLed("mRPMLedTable")

	elseif mSpdLimitMethod == 4 then
		-- speedlimiter led + RPM + digits blinking, method 4
		if GetTicks() > mOldTickCount then
			mSpdLmtBlink = mSpdLmtBlink + 1
		end
		
		if mSpdLmtBlink >= mBlinkTime then
			mSpdLmtBlink = 0
		end

		if mSpdLmtBlink <= (mBlinkTime / 2) then
			
			if mLimiterChar then SetGearDigit("L") end
			SetDigitsAllowed(true)

			for i = 0,2 do 
				led = led.format("RPM%d",i)
				mRPMLedTable[led] = 0
			end
			for i = 10,12 do 
				led = led.format("RPM%d",i)
				mRPMLedTable[led] = 0
			end
			
			if not mSpdLmtRPMLedOnly then 
				SetWarnLed(mSpeedLimiterLED, 0)
			end	
		end

		if mSpdLmtBlink > (mBlinkTime / 2) then
			if mLimiterChar then SetGearDigit(" ") end
		
			SetDigitsAllowed(false)
			
			SetLeftDigits  (mLeftSpdLmtText)
			SetRightDigits (mRightSpdLmtText)

			for i = 0,2 do 
				led = led.format("RPM%d",i)
				mRPMLedTable[led] = 1
			end
			for i = 10,12 do 
				led = led.format("RPM%d",i)
				mRPMLedTable[led] = 1
			end
			if not mSpdLmtRPMLedOnly then 
				SetWarnLed(mSpeedLimiterLED, 0)
			end	
		end
		SetRPMLed("mRPMLedTable")

	elseif mSpdLimitMethod == 5 or  mSpdLimitMethod == 6 then
		-- speedlimiter led + digits blinking, method 5
		-- speedlimiter led + RPM fixed + Digits blinking, method 6
		if GetTicks() > mOldTickCount then
			mSpdLmtBlink = mSpdLmtBlink + 1
		end

		if mSpdLmtBlink >= mBlinkTime then
			mSpdLmtBlink = 0
		end
		
		if mSpdLmtBlink <= (mBlinkTime / 2) then
			if mLimiterChar then SetGearDigit("L") end

			SetDigitsAllowed (true)
			
			local state = 0
			if mSpdLimitMethod == 6 then state = 1 end
			initLedTable(mRPMLedTable, state)
			
			if not mSpdLmtRPMLedOnly then 
				SetWarnLed(mSpeedLimiterLED, 0)
			end	
		end

		if mSpdLmtBlink > (mBlinkTime / 2) then
			if mLimiterChar then SetGearDigit(" ") end
			
			SetLeftDigits  ( mLeftSpdLmtText)
			SetRightDigits ( mRightSpdLmtText)
		
			SetDigitsAllowed(false)
			
			local state = 0
			if mSpdLimitMethod == 6 then state = 1 end
			initLedTable(mRPMLedTable, state)
			
			if not mSpdLmtRPMLedOnly then 
				SetWarnLed(mSpeedLimiterLED, 0)
			end
		end
		SetRPMLed("mRPMLedTable")

	else
		return 0
	end
	
	-- timebase
	if GetTicks() > mOldTickCount then
		mOldTickCount = GetTicks() + 20
	end
	return 1
end
 
