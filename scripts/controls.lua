-- SLIMax Mgr Lua Script v2
-- Copyright (c)2011-2013 by EK and Zappadoc - All Rights Reserved.
-- Use this script to control any button or switch

-- param deviceIdx = (see mDeviceType table)
-- param ctrlType = type of ctrl, switch (0) or button (1)
-- param ctrlPos = ctrl index, switch from 1 to 6 and button from 1 to n 
-- param value = ctrl value, button down (>0) or up (==0) and switch from 1 to 12 
-- return 0 to give the control to SLIMax Mgr
-- return 1 to force the update of device
function controlsEvent(deviceIdx, ctrlType, ctrlPos, value, funcIndex)
	-- call custom script
	local result = custom_controlsEvent(deviceIdx, ctrlType, ctrlPos, value, funcIndex)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- call global custom script
	result = global_custom_controlsEvent(deviceIdx, ctrlType, ctrlPos, value, funcIndex)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue

	local loopFlag = false
	local dev = ""
	dev = mDeviceType[deviceIdx]
	if dev == nil then dev = "none" end
	-- switches and buttons
	-- ctrl change event
	local state = 0
	local leftStr = ""
	local rightStr = ""
	
	local oldTcks = GetTicks()
	
	local delay = 600
	if funcIndex ~= -1 then
		if IsSLIFunction("maxgear", funcIndex) and mMaxGearFeedbackAllowed then 
			-- max gear switch 
			-- value from 4 to 7
			local mxgear = 0
			mxgear = GetCarInfo("maxgear")
			
			leftStr = " GEAR "
			rightStr = rightStr.format( "MAX  %1d", mxgear) 
			
			-- set digits
			UpdateSLIProDigits(leftStr, rightStr)
			SLISendReport(0)

			state = 1
			-- set timeout
			oldTcks = GetTicks() + delay

		elseif IsSLIFunction("brightness", funcIndex) and mBrightnessFeedbackAllowed then 
			-- brightness feedback (switch or buttons)
			BrightnessFeedBack(dev)
			
			leftStr = "888888"
			rightStr = "888888" 

			SLISendReport(0)
			
			state = 1
			-- set timeout
			oldTcks = GetTicks() + delay

		elseif IsSLIFunction("ospfactor", funcIndex) and mOSPFeedbackAllowed then 
			-- OSP Factor feedback
			leftStr = "OSP:   "
			
			local ospf = GetContextInfo("ospfactor")
			rightStr = rightStr.format(" %03d  ", ospf) 
			
			-- set digits		
			UpdateSLIProDigits(leftStr, rightStr)
			SLISendReport(0)
			
			state = 1
			-- set timeout
			oldTcks = GetTicks() + delay
			
		elseif IsSLIFunction("speedmetric", funcIndex) and mUnitFeedbackAllowed then 
			-- metric KPH or MPH
			local spdm = false
			spdm = GetContextInfo("speedmetric")
			
			leftStr =  "SPD:   "
			rightStr = " KPH  "
			if spdm then rightStr  = " MPH  " end

			-- set digits
			UpdateSLIProDigits(leftStr, rightStr)
			SLISendReport(0)
			
			state = 1
			-- set timeout
			oldTcks = GetTicks() + delay
			
		elseif IsSLIFunction("rightdigits", funcIndex) and mRightDigitsFeedbackAllowed then 
			-- rightdigits feedback
			local info = false
			info = GetContextInfo("rightdigits")
			leftStr = leftStr.format("Btn: %02d", ctrlPos) 
			rightStr = rightStr.format("  %02d  ", info) 
		
			-- set digits
			UpdateSLIProDigits(leftStr, rightStr)
			SLISendReport(0)
			
			state = 1
			-- set timeout
			oldTcks = GetTicks() + delay
			
		elseif IsSLIFunction("leftdigits", funcIndex) and mLeftDigitsFeedbackAllowed then 
			-- leftdigits feedback
			local info = false
			info = GetContextInfo("leftdigits")
			leftStr = leftStr.format("Btn: %02d", ctrlPos) 
			rightStr = rightStr.format("  %02d  ", info) 
		
			-- set digits
			UpdateSLIProDigits(leftStr, rightStr)
			SLISendReport(0)
			
			state = 1
			-- set timeout
			oldTcks = GetTicks() + delay
			
		elseif IsSLIFunction("lowfuel", funcIndex) and mLowFuelFeedbackAllowed then 
			-- lowfuel feedback
			-- get the option
			local info = false
			info = GetContextInfo("lowfuel")
			
			-- set digits
			leftStr = leftStr.format(" FUEL ", ctrlPos) 
			rightStr = rightStr.format("LPS %02d", info) 
		
			UpdateSLIProDigits(leftStr, rightStr)
			SLISendReport(0)
			
			state = 1
			-- set timeout
			oldTcks = GetTicks() + delay
		end
	else
		if ctrlType == 0 then 
			-- switch 	
			
			if mDemoMapToKeyAllowed and ctrlPos == mDemoMapToKeySwitch then
				-- demo script 
				-- mapping a keystroke (see global.lua to set the targeted switch)
				local key = ""
				if value == 2 then key = "v" end
				-- if value == 3 then key = "K" end
				-- if value == 4 then key = "v" end
				-- if value == 5 then key = "F5" end
				if key ~= "" then
					-- params: key, delay, modifier
					SetKeystroke(key, 100, "")
				end
			end
			
			if mSwitchFeedbackAllowed then
				-- general switch feedback (see global.lua to activate)
				leftStr = leftStr.format("S%1d :   ", ctrlPos) 
				rightStr = rightStr.format("  %02d  ", value) 
			
				-- set digits
				UpdateSLIProDigits(leftStr, rightStr)
				SLISendReport(0)
				
				state = 1
				-- set timeout
				oldTcks = GetTicks() + delay
			end
		elseif ctrlType == 1 then
			if ctrlPos == mDumpLapButton  and dev == "SLI-PRO" and value>0 and mDumpLapAllowed then
				-- sample script using button of SLI-PRO to
				-- toggle telemetry dumplastlap flag (true or false)
				-- telemetry logs are stored in telemetry directory
				-- after each completed lap
				local dlap = GetContextInfo("dumplastlap")
				local flag = not dlap;
				TelemetryTools("dumplastlap", flag);
				-- set digits
				leftStr = "tELENN" 
				if flag then
					rightStr = " SAVE "
				else
					rightStr = " FALSE"
				end
				UpdateSLIProDigits(leftStr, rightStr)
				SLISendReport(0)
				
				state = 1
				-- set timeout
				oldTcks = GetTicks() + delay
			end
		end
	end
	
	if state >= 1 then
		-- loop until timeout
		local newtcks = GetTicks()	
		loopFlag = true
		while(oldTcks > newtcks and loopFlag ) do
			SLISleep(10)
			newtcks = GetTicks()
			SetLeftDigits(leftStr)
			SetRightDigits(rightStr)
			SLISendReport(0)
		end
		
		-- cleanup device 
		oldTcks = 0
		state = 0
		toggleAllLed(0)
		UpdateSLIProDigits("      ", "      ")	
		SLISendReport(0)
		
		-- allow Mgr to display info on digits
		SetDigitsAllowed(true)		
	end

	-- skip
	return 0
end

-- set leds, digits, parts to cleanup and timeout
function UpdateSLIProDigits(leftInfo, rightInfo)
	-- do not refresh digits until timeout
	SetDigitsAllowed(false)

	-- set digits string
	SetLeftDigits(leftInfo)
	SetRightDigits(rightInfo)
end

-- brightness feedback function
function BrightnessFeedBack(device)
	-- set all leds
	toggleAllLed(1)	
		
	if device == "SLI-PRO" then
		-- SLI-PRO Device
		-- set digits
		UpdateSLIProDigits("888888", "888888")
	end
end
