-- SLIPRO Lua Script v2.5
-- Copyright (c)2011-2013 by EK and Zappadoc - All Rights Reserved.
-- Use this script to build all functions associated with 
-- left and right panels of SLIPRO device 
-- changed by Zappadoc - 2012-10-29

-- ============================================================
-- Left and Right Panel functions
-- ============================================================
function isAppIRacing(sim)
	if sim == "iRacingSim.exe" or sim == "iRacingSim64.exe" then
		return true
	end
	return false
end
function isAppRFactor(sim)
	if sim == "rFactor.exe" or sim == "rFactor2.exe" then
		return true
	end
	return false
end

-- IN function of left panel
-- param: recieve from SLI Manager the current switch position 
function leftDigitsEvent(swFunction)
	swValue = swFunction + 1
	-- call custom script
	local result = custom_leftDigitsEvent(swValue)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- call global custom script
	result = global_custom_leftDigitsEvent(swValue)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
    
	local lPanel = ""
    local inf = ""
	local unit = false
    local spd = 0.0
	-- get speed in kph or mph (use "rawspeed" to get value in meter/sec)
	local spd = GetCarInfo("speed")
	-- get current display unit metric or imperial
	unit = GetContextInfo("speedmetric")
	-- check if quick info button is down
	local qi = false
	qi = GetContextInfo("quickinfobutton")
	if qi == nil or qi == 0 then qi = false end
	if qi then
		-- get index for left panel
		local qiLeft = GetContextInfo("quickinfoleft")
		if qiLeft == nil then qiLeft = 1 end
		-- force position to match QI preference
		swValue = qiLeft
	end
	-- get current simulation name
	local sim = GetContextInfo("simulation")
	
	--print ( "swValue: " .. swValue .. "\n")
	
	-- check postion and compute left panel string
	if swValue == 1 then
		-- speed only
		lPanel = string.format("  %3.0f ", spd )

	elseif swValue == 2 then
		-- fuel:speed
		local fuel = GetCarInfo("fuel")
		if fuel ~= nil then
			fuel = GetFuel(fuel, unit)
			if fuel >= 100 then
				lPanel = string.format("%03d:%3d", round(fuel), spd)
			elseif fuel >= 10 then
				lPanel = string.format("F%02d:%3d", round(fuel), spd)
			else
				lPanel = string.format("F%1.1f:%3d", fuel, spd)
			end
		end
    elseif swValue == 3 then
		-- position:speed
		inf = GetContextInfo("position")
		if inf ~= nil then
			if inf >= 100 then
				 lPanel = string.format("%03d:%3.0f", inf, spd)
			else
				 lPanel = string.format("P%02d:%3.0f", inf, spd)
			end		   
  		end
   elseif swValue == 4 then
		-- laps completed:speed
		inf = GetContextInfo("laps")
		if inf ~= nil then
			-- if more then 99 laps
			if inf >= 100 then
				lPanel = string.format("%03d:%3.0f", inf, spd)
			else
				lPanel = string.format("L%02d:%3.0f", inf, spd)
			end
 		end
    elseif swValue == 5 then
		-- sector:speed
		inf = GetCarInfo("sector")
		if inf ~= nil then
			-- check if sector > 9
			if inf >9 then
				lPanel = string.format("S%02d:%3.0f", inf, spd)
			else
				lPanel = string.format("S%01d :%3.0f", inf, spd)
			end
 		end
   elseif swValue == 6 then
		-- laps completed:total laps if available
		inf = GetContextInfo("laps")
		if inf ~= nil then
		    local tl = GetContextInfo("lapscount")
		    if tl < 1 then  tl = 0 end
			-- if more then 99 laps
			if inf >= 100 or tl >= 100 then
				lPanel = string.format("%03d:%03d", inf, tl)
			else
				lPanel = string.format("L%02d:t%02d", inf, tl)
			end
		end
    elseif swValue == 7 then
		-- water temp
		inf = GetCarInfo("watertemp")
		if inf ~= nil then
			inf = GetTemp(inf, unit)
		    lPanel = string.format("H2o:%2.1f", inf)
 		end
    elseif swValue == 8 then
		-- oil temp
		inf = GetCarInfo("oiltemp")
		if inf ~= nil then
			inf = GetTemp(inf, unit)
		    lPanel = string.format("OIL:%2.1f", inf)
 		end
		
    elseif swValue == 9 then
		-- wheels temp if available
		inf = GetCarInfo("wheeltempfrontleft")
		if inf ~= nil then
			-- if rFactor convert Kelvin to Celsius (see global.lua)
			if isAppRFactor(sim) then  inf = KtoC(inf) end
		    lPanel = string.format("tFL:%3.0f", inf)
 		end
    elseif swValue == 10 then
		inf = GetCarInfo("wheeltempfrontright")
		if inf ~= nil then
			if isAppRFactor(sim) then  inf = KtoC(inf) end
		    lPanel = string.format("tFr:%3.0f", inf)
 		end
    elseif swValue == 11 then
		inf = GetCarInfo("wheeltemprearleft")
		if inf ~= nil then
			if isAppRFactor(sim) then  inf = KtoC(inf) end
		    lPanel = string.format("trL:%3.0f", inf)
 		end
    elseif swValue == 12 then
		inf = GetCarInfo("wheeltemprearright")
		if inf ~= nil then
			if isAppRFactor(sim) then  inf = KtoC(inf) end
		    lPanel = string.format("trr:%3.0f", inf)
 		end
	
  elseif swValue == 13 then
		-- wheels pressure if available
		inf = GetCarInfo("wheelpressfrontleft")
		if inf ~= nil then
			-- convert to psi
			lPanel = string.format("PFL:%2.1f", inf / 6.88)
 		end
    elseif swValue == 14 then
		inf = GetCarInfo("wheelpressfrontright")
		if inf ~= nil then
		    lPanel = string.format("PFr:%2.1f", inf / 6.88)
 		end
    elseif swValue == 15 then
		inf = GetCarInfo("wheelpressrearleft")
		if inf ~= nil then
		    lPanel = string.format("PrL:%2.1f", inf / 6.88)
 		end
    elseif swValue == 16 then
		inf = GetCarInfo("wheelpressrearright")
		if inf ~= nil then
		    lPanel = string.format("Prr:%2.1f", inf / 6.88)
 		end
		
   elseif swValue == 17 then
		-- brakes temp if available
		inf = GetCarInfo("braketempfrontleft")
		if inf ~= nil then
			if isAppRFactor(sim) or sim == "GTR2.exe" then  inf = KtoC(inf) end
		    lPanel = string.format("BFL:%3.0f", inf)
 		end
    elseif swValue == 18 then
		inf = GetCarInfo("braketempfrontright")
		if inf ~= nil then
			if isAppRFactor(sim) or sim == "GTR2.exe" then  inf = KtoC(inf) end
		    lPanel = string.format("BFr:%3.0f", inf)
 		end
    elseif swValue == 19 then
		inf = GetCarInfo("braketemprearleft")
		if inf ~= nil then
			if isAppRFactor(sim) or sim == "GTR2.exe" then  inf = KtoC(inf) end
		    lPanel = string.format("BrL:%3.0f", inf)
 		end
    elseif swValue == 20 then
		inf = GetCarInfo("braketemprearright")
		if inf ~= nil then
			if isAppRFactor(sim) or sim == "GTR2.exe" then  inf = KtoC(inf) end
		    lPanel = string.format("Brr:%3.0f", inf)
 		end
	elseif swValue == 21 then
		-- position
		local pos = GetContextInfo("position")
		lPanel = string.format("  P%02d ",  pos )
			
	elseif swValue == 22 then
		-- logo
		lPanel = "SLIMAX"
			
	elseif swValue == 23 then
		-- rpm
		local rpm = GetCarInfo("rpm")
		lPanel =  string.format("%5d ", rpm)
					
	elseif swValue == 24 then
		-- track size
		local trcksz = GetContextInfo("tracksize")
		lPanel =  string.format("%5d ", trcksz)	
			
	elseif swValue == 25 then
		-- 25.fuel
		local fuel = GetCarInfo("fuel")
		if fuel ~= nil then
			fuel = GetFuel(fuel, unit)
			if fuel >= 100 then
				lPanel = string.format(" F%03d ", round(fuel))
			elseif fuel >= 10 then
				lPanel = string.format(" F%02d  ", round(fuel))
			else
				lPanel = string.format(" F%1.1f  ", fuel)
			end
		end
		
	elseif swValue == 26 then
		-- 26.distance
		local dist = GetContextInfo("lapdistance")
		lPanel = string.format(" %5d", dist )
		
	elseif swValue == 27 then
		--27.lap completed
		local laps = GetContextInfo("laps")
		lPanel = string.format("  %3d ", laps )
		
	elseif swValue == 28 then
		--28.total laps
		local lpcnt = GetContextInfo("lapscount")
		lPanel = string.format("  %3d ", lpcnt )
		
	elseif swValue == 29 then
		--29.sector
		local sect = GetCarInfo("sector")
		-- check if sector > 9
		if sect >9 then
			lPanel = string.format("  S%02d ", sect)
		else
			lPanel = string.format("  S%01d  ", sect)
		end

	elseif swValue == 30 then
		--30.kers
		local kers = GetCarInfo("kers")
		lPanel = string.format("  E:%3d", round(kers/1000))
		
	elseif swValue == 31 then
		--31.kers max 
		local kmx = GetCarInfo("kersmax")
		lPanel = string.format("  E:%03d", round(kmx/1000))
		
	elseif swValue == 32 then
		--32.drs
		local drs = GetCarInfo("drs")
		if drs == 1 then
			lPanel = "DrS:ON " 
		else
			lPanel = "DrS:OFF"
		end
		
	elseif swValue == 33 then
		--33.kers percent
		local kers = GetCarInfo("kers")
		lPanel = string.format("  E:%3d", round((kers/1000)/4))
		
	elseif swValue == 34 then
		--33.kers percent:speed
		local kers = GetCarInfo("kers")
		lPanel = string.format("%3d:%3.0f", round((kers/1000)/4), spd)
		
	else
		    lPanel = "-:--.---"
	end
	-- send string to sli manager
	SetLeftDigits( lPanel ) 
	return 1
end

-- IN function of right panel
-- param: recieve from SLI Manager the current switch position
function rightDigitsEvent(swFunction)
	swValue = swFunction + 1
	-- call custom script
	local result = custom_rightDigitsEvent(swValue)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- call global custom script
	result = global_custom_rightDigitsEvent(swValue)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue

	local hr = 0
	local mn = 0
	local sc = 0
	local ms = 0
	local hd = 0
	local rPanel = ""
	local lpt = 0.0
	local diffFlag = false
	local unit = false
	
	-- is OSP Tracking ON
	local ospt = false
	ospt = GetContextInfo("osptracking")
	if ospt == nil then ospt = false end
	if ospt then
		swValue = 23
	end
	
	-- lap finished, display lap time a few seconds
	local dlt = false
	dlt = GetContextInfo("displaylaptime")
	if dlt == nil then dlt = false end
	if dlt then
		swValue = 3
	end
	
	-- check if quick info button is down
	local qi = false
	qi = GetContextInfo("quickinfobutton")
	if qi == nil or qi == 0 then qi = false end
	if qi then
		-- get index for right panel
		local qiRight = GetContextInfo("quickinforight")
		if qiRight == nil then qiRight = 1 end
		swValue = qiRight
	end
	-- get current display unit metric or imperial
	unit = GetContextInfo("speedmetric")
	
	-- get sim name
	local sim = GetContextInfo("simulation")
	if sim ~= nil then
		if swValue == 1 then
			-- current lap time
			lpt = GetTimeInfo("laptime")
		elseif swValue == 2 then
			-- best lap time (PB)
			lpt = GetTimeInfo("bestlaptime")
		elseif swValue == 3 then
			-- last lap time
			lpt = GetTimeInfo("lastlaptime")
			
		elseif swValue >= 4 and swValue <= 10 and isAppIRacing(sim) then
			-- iRacing partials
			local ts = GetContextInfo("partialcount")
			local sector = GetCarInfo("sector")
			if ts ~= nil and ts > 0 then
				if swValue == 4 then
				   lpt = GetPartialTimeInfo("currentpartial", sector)
				elseif swValue == 5 then
					lpt = GetPartialTimeInfo("bestpartial", sector)
				elseif swValue == 6 then
					lpt = GetPartialTimeInfo("sessionbest", sector)
				elseif swValue == 7 then
					diffFlag = true
					lpt = GetPartialTimeInfo("diffbestpartial", sector)
				elseif swValue == 8 then
					diffFlag = true
					lpt = GetPartialTimeInfo("diffsessionbest", sector)
				elseif swValue == 9 then
					diffFlag = true
					lpt = GetPartialTimeInfo("diffoptimalpartial", sector)
				elseif swValue == 10 then
					diffFlag = true
					lpt = GetPartialTimeInfo("diffsessionoptimal", sector)
				end
			else
				lpt = 0.0
			end

		elseif swValue == 11 then
			-- real time diff vs your best
			diffFlag = true
			lpt = GetTimeInfo("realdiffbest")

		elseif swValue == 12 then
			-- real time diff vs your last
			diffFlag = true
			lpt = GetTimeInfo("realdifflast")

		elseif swValue == 13 then
			-- current sector
			local sector = GetCarInfo("sector")
			if sector == 1 then
				lpt = GetTimeInfo("sector1")
			elseif sector == 2 then
				lpt = GetTimeInfo("sector2")
			else
				-- sector 3
				local s1 = GetTimeInfo("sector1")
				local s2 = GetTimeInfo("sector2") 
				local lt = GetTimeInfo("laptime") 
				lpt = lt - (s1 + s2)
			end
		elseif swValue == 14 then
			-- best sector 1
			lpt = GetTimeInfo("bestsector1")
		elseif swValue == 15 then
			-- best sector 2
			lpt = GetTimeInfo("bestsector2")
		elseif swValue == 16 then
			-- last sector 1
			lpt = GetTimeInfo("lastsector1")
		elseif swValue == 17 then
		   	-- last sector 2
			lpt = GetTimeInfo("lastsector2")
		elseif swValue == 18 then
			-- position
			local pos = GetContextInfo("position")
			if pos >= 100 then
				rPanel = string.format(" P%03d ",  pos )
			else
				rPanel = string.format("  P%02d ",  pos )
			end		   
			SetRightDigits( rPanel ) 
			return 1
		elseif swValue == 19 then
			-- logo
			rPanel = "SLIMAX"
			SetRightDigits( rPanel ) 
			return 1
		elseif swValue == 20 then
			-- speed on right panel
			local spd = GetCarInfo("speed")
			rPanel = string.format("  %3d ", spd )
			SetRightDigits( rPanel ) 
			return 1
		elseif swValue == 21 then
			-- time remaining if available
			lpt = GetTimeInfo("timeremaining")

			-- explod time
			hr, mn, sc, hd, ms = timeDispatcher(lpt)
			if hr > 0 then
				rPanel =  string.format( "%02dh%02d ", hr, mn) 
			else
				rPanel =  string.format( " %02d.%02d ", mn, sc) 
			end			
			SetRightDigits( rPanel ) 
			return 1
		elseif swValue == 22 then
			-- PC system time
			lpt = GetTimeInfo("systemtime")
			-- explod time
			hr, mn, sc, hd, ms = timeDispatcher(lpt)
			rPanel =  string.format( " %02d.%02d ", hr, mn) 
			SetRightDigits( rPanel ) 
			return 1
		elseif swValue == 23 then
			-- rpm:gear
			local rpm = GetCarInfo("rpm")
			if rpm == nil then 
				return 1
			end
			local gear = GetCarInfo("gear")
			if gear ~= nil then
				rPanel =  string.format("%5d:%d", rpm, gear)
				SetRightDigits( rPanel ) 
			end
			return 1
		elseif swValue == 24 then
			-- time elapsed if available
			lpt = GetTimeInfo("timetotalelapsed")

			-- explod time
			hr, mn, sc, hd, ms = timeDispatcher(lpt)
			if hr > 0 then
				rPanel =  string.format( "%02dh%02d ", hr, mn) 
			else
				rPanel =  string.format( " %02d.%02d ", mn, sc) 
			end			
			SetRightDigits( rPanel ) 
			return 1
		elseif swValue == 25 then
			-- 25.fuel
			local fuel = GetCarInfo("fuel")
			if fuel ~= nil then
				fuel = GetFuel(fuel, unit)
				if fuel >= 100 then
					rPanel = string.format(" F%03d ", round(fuel))
				elseif fuel >= 10 then
					rPanel = string.format(" F%02d  ", round(fuel))
				else
					rPanel = string.format(" F%1.1f  ", fuel)
				end
				
				SetRightDigits( rPanel ) 
				return 1
			end
			
		elseif swValue == 26 then
			-- 26.distance
			local dist = GetContextInfo("lapdistance")
			rPanel = string.format(" %5d", dist )
			SetRightDigits( rPanel ) 
			return 1
			
		elseif swValue == 27 then
			--27.lap completed
			local laps = GetContextInfo("laps")
			rPanel = string.format("  %3d ", laps )
			SetRightDigits( rPanel ) 
			return 1
			
		elseif swValue == 28 then
			--28.total laps
			local lpcnt = GetContextInfo("lapscount")
			rPanel = string.format("  %3d ", lpcnt )
			SetRightDigits( rPanel ) 
			return 1
			
		elseif swValue == 29 then
			--29.sector
			local sect = GetCarInfo("sector")
			-- check if sector > 9
			if sect >9 then
				rPanel = string.format(" S%02d  ", sect)
			else
				rPanel = string.format(" S%01d   ", sect)
			end
			SetRightDigits( rPanel ) 
			return 1

		elseif swValue == 30 then
			--30.kers
			local kers = GetCarInfo("kers")
			rPanel = string.format(" E%03d ", round(kers/1000) )
			SetRightDigits( rPanel ) 
			return 1
		
		elseif swValue == 31 then
			--31.kers max
			local kmx = GetCarInfo("kersmax")
			rPanel = string.format(" E%03d ", round(kmx/1000) )
			SetRightDigits( rPanel ) 
			return 1
			
		elseif swValue == 32 then
			--32.drs
			rPanel = "DrS.OFF"
			local drs = GetCarInfo("drs")
			if drs >= 1 then
				rPanel = "DrS. ON" 
			end
			SetRightDigits( rPanel ) 
			return 1
			
		elseif swValue == 33 then
			--33.kers percent
			local kers = GetCarInfo("kers")
			rPanel = string.format(" %3d  ", round((kers/1000)/4))
			SetRightDigits( rPanel ) 
			return 1
					
		elseif swValue == 34 then
			--34 track size
			local trcksz = GetContextInfo("tracksize")
			rPanel =  string.format("%5d ", trcksz)	
			SetRightDigits( rPanel ) 
			return 1

		elseif swValue == 35 then
			--35 last sector 1, 2 and 3
			local sector = GetCarInfo("sector")
			if sector == 1 then
				-- sector 3
				local ls1 = GetTimeInfo("lastsector1")
				local ls2 = GetTimeInfo("lastsector2") 
				local lt = GetTimeInfo("lastlaptime") 
				if ls1 > 0 and ls2 > 0 then
					lpt = lt - (ls1 + ls2)
				end
			elseif sector == 2 then
				local ls1 = GetTimeInfo("lastsector1")
				lpt = ls1
			else
				-- sector 3
				local ls2 = GetTimeInfo("lastsector2") 
				lpt = ls2
			end			
			
		else
			-- none
			rPanel = "-:--.---"
			SetRightDigits( rPanel ) 
			return 1
		end
	end
	
	if lpt == nil then return 0 end
	
	local c = " "
	if lpt < 0 then
		c = "-"
	end
	-- explod time
    hr, mn, sc, hd, ms = timeDispatcher(lpt)
	 -- print("lpt: " .. lpt .. " m: " .. mn .. " - s: " .. sc .. " - ms: " .. ms .. )
			
	if diffFlag then
		-- 
		if GetTicks() > (mDeltaTimeDelay + mDeltaTimeOldTicks ) then
			mDeltaTimeOldTicks = GetTicks()
			if lpt == -1 or (mn + sc + ms) == 0.0000 then
				mDeltaTimeBackup =  " --.---" 
			elseif mn > 0 then
				mDeltaTimeBackup =  string.format( "%s%2d.%02d.%01d", c, mn, sc, ms)
			 else
				mDeltaTimeBackup =  string.format( "%s%2d.%03d", c, sc, ms)
			 end
		end
		rPanel = mDeltaTimeBackup
		
	else
		if lpt == -1 or (mn + sc + ms) == 0.0000 then
			rPanel =  "-:--.---" 
		elseif mn < 10 then
			rPanel =  string.format( "%1d:%02d.%03d", mn, sc, ms)
		elseif hr > 0 then
			rPanel =  string.format( " %02d.%02d ", hr, mn)   
		 else
			rPanel =  string.format( " %02d.%02d.%01d", mn, sc, ms)
		 end
	 end
	 -- send time to sli manager
	 SetRightDigits( rPanel ) 
	return 1
end

