SLIMax Manager II 2.2.4.5 Scripting Information - PART OF SLIMax Manager pkg
Copyright (c)2011-2013 by EK & Zappadoc - All Rights Reserved.
updated 2013-01-16 by Zappadoc

** ALL INFORMATION BELOW ARE SUBJECT TO CHANGE WITHOUT NOTICE **

=================================
Script Engine based on Lua 5.1.4

License for Lua 5.1.4 and later versions
Copyright � 1994�2011 Lua.org, PUC-Rio.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

=================================
Lua website
http://www.lua.org/

Reference manual (v5.1):
http://www.lua.org/manual/5.1/


=================================
Events received from SLIMax Manager
=================================

function custom_initEvent(scriptFile)
param = custom script file name
Notes: this event is triggered at startup or when loading your custom script, 
this is the good opportunity to init your preferences and global vars

All event functions below must return 1 if the event has been fully managed or
0 (zero) to skip and give control to SLIMax Mgr

---------------------------------------------
function shiftLightsMethodEvent(idx) and custom_shiftLightsMethodEvent
function shiftLightsBU0710Event(idx) and custom_shiftLightsBU0710Event
param = current method index
Notes: shiftlights method event, computes all shiftlights methods
(see shiftlights.lua script)

---------------------------------------------
function ospMethodEvent(idx) and custom_ospMethodEvent
param = current method index
Notes: OSP method event, computes all Optimal Shift-Points methods
(see osp.lua script)

---------------------------------------------
function spdLmtMethodEvent(idx) and custom_spdLmtMethodEvent
param = current method index
Notes: speedlimiter method event, computes all speedlimiter methods
(see speedlimiter.lua script)

---------------------------------------------
function leftDigitsEvent(swPosition) and custom_leftDigitsEvent
function rightDigitsEvent(swPosition) and custom_rightDigitsEvent
param = switch position
Notes: SLI-PRO Left and Right Panel functions events, computes data to be
displayed on left and rights digits of SLI-PRO device
(see slipro.lua script)

---------------------------------------------
function controlsEvent(deviceIdx, ctrlType, ctrlPos, value, funcIndex) and custom_controlsEvent
params = see the script
Notes: Controls Event, Manage buttons and switches. (see controls.lua script)

---------------------------------------------
function deviceReport(devType) and custom_deviceReport
param = the current targeted SLI device
Notes: This function is called periodically and outputs the previously processed data to the device when it returns
back to the manager, if needed, patch any previous function here to
display additional info or to change everything. (see devhook.lua script)

---------------------------------------------
function gearEvent(gear) and custom_gearEvent
param = current seleted gear ( -1 = reverse, 0 = neutral, 1 ... 7)
Notes: gear event, show the current selected gear on central digit
(see gear.lua script)

---------------------------------------------
function enterSessionEvent(devType) and custom_enterSessionEvent
param = current device (SLI-M, SLI-PRO or BU0710)
Notes: Triggered when the session starts
(see enter_exit_session.lua script)

---------------------------------------------
function exitSessionEvent(devType) and custom_exitSessionEvent
param = current device (SLI-M, SLI-PRO or BU0710)
Notes: Triggered when the session ends
(see enter_exit_session.lua script)

---------------------------------------------
function ledEvent(idx, ledFunction, state) and custom_ledEvent
param = index of the led, std state of the led and corresponding function
1=LowFuel
2=TC
3=ABS
4=Green Flag
5=Yellow flag
6=Red Flag
7=Over Heating
8=Damage
9=Pit request
10=Power
11=Rev Limit
12=HeadLights
13=Blue Flag
14=DRS
15=Safe Prefs
Notes: patch std led behaviors (i.e. tc, abs, lowfuel,
 dammage, rev limit, drs, pit request, flags, ... )
IMPORTANT: your script must be optimized as the function is
called several time by second to report all activated leds
(see led.lua script)

-----------------------------------------------
All functions starting with custom_ can created 
to change std behaviors and can be associated 
with your custom settings.
(see COMBO SETUP in forum)
-----------------------------------------------


============================================
functions to change SLIMax Manager information
============================================

---------------------------------------------
SetBU0710Led(int)
param integer = number from 0 to 10
set leds of BU0710 device (e.g. see shiftlights.lua script)

---------------------------------------------
SetGeadDigit(string)
param string = one char string (i.e. "r" or "n" or "1")
set gear digit of current targeted SLI device (e.g. see gear.lua script)

---------------------------------------------
SetRPMLed(string)
param string = name of lua table (default mRPMLedTable)
set RPM led of current targeted SLI device, SLI-M or SLI-PRO (e.g. see shiftlights.lua script)

---------------------------------------------
SetWarnLed(idx, val)
param integer  = index of led from 1 to 6
param integer  = value 1 or 0
set the 6 extra leds of current targeted SLI device (SLI-M or SLI-PRO)

---------------------------------------------
SetExtLed(idx, val)
param integer = index of led from 1 to 5
param integer  = value 1 or 0
set the 5 external leds of current targeted SLI device (SLI-M or SLI-PRO)

---------------------------------------------
SetOSPStatus(boolean)
param boolean = true or false
in Optimal ShiftPoints (OSP) Methods compute your own OSP curve and set
the corresponding OSP state with this function (true or false - see osp.lua script)

---------------------------------------------
========== SLI-PRO DIGITS ==========
SetRightDigits(string)
SetLeftDigits(string)
param string = 6 chars string + delimiter
set the alpha-numerical text (including the . and : delimiter) to the right
and left digits (see slipro.lua script)

---------------------------------------------
SetDigitsAllowed(boolean)
param boolean = true or false
by default the 6 + 6 SLI-PRO digits are showed but sometime we need to temporarily
hide them to set our custom information, pass false to this function to hide digits
and true to show them again

---------------------------------------------
SetKeystroke(keycode, delay, modifier)
param string or integer = the key as a string (see the special keycode table below ) or Microsoft VKey code as integer
param int = the delay from 50 to 1000 ms
param modifier = SHIFT or CONTROL or ALT
This function is experimental and unsupported

SPECIAL KEYCODE TABLE:
 BACKSPACE
 DEL
 INSERT
 PRTSCREEN
 END
 HOME
 SCROLL
 PGDOWN
 PGUP
 PAUSE
 UP
 DOWN
 LEFT
 RIGHT
 F1
 F10
 F11
 F12
 F13
 F14
 F15
 F16
 F2
 F3
 F4
 F5
 F6
 F7
 F8
 F9
 NUMLOCK
 NUMPAD0
 NUMPAD1
 NUMPAD2
 NUMPAD3
 NUMPAD4
 NUMPAD5
 NUMPAD6
 NUMPAD7
 NUMPAD8
 NUMPAD9
 ENTER
 ESC
 TAB
 ARROWUP
 ARROWDOWN
 ARROWLEFT
 ARROWRIGHT
 
Modifier:

SHIFT
CONTROL
ALT

-----------------------------------------------------------------------------------
TelemetryTools(selector, value)
Param "value" boolean activate or deactivate the function
Param "selector" (string), this string contains the following value:
"dumplastlap" if it's activated it creates a telemetry log after each lap

-----------------------------------------------------------------------------------
SLISleep(ms)

-----------------------------------------------------------------------------------
SLISendReport()

-----------------------------------------------------------------------------------
SetGlobalBrightness(int)

-----------------------------------------------------------------------------------
SetABSBrightLevel(int)

-----------------------------------------------------------------------------------
SetTCBrightLevel(int)

---------------------------------------------
SetMaxGear(int)
param integer = from 4 to 7
Set the number of gears available

---------------------------------------------
SetDeltaTimeDelay(int)
param integer = from 0 to 5000
set interval to update the display for easy reading of time
the deltatime delay is in ms (1000 = 1s )

=========================================
functions to get SLI Manager Information
=========================================

GetContextInfo("selector")
GetCarInfo("selector")
GetTimeInfo("selector")
GetPartialTimeInfo("selector", index)
IsSLIFunction("selector", function_index)

-----------------------------------------------------------------------------------
GetContextInfo("selector")
returns contextual info (internal function)
Param "selector" (string), this string contains the following value:

IMPORTANT ** THESE VALUES ARE NOT AVAILABLE IN ALL SIMULATIONS ** IMPORTANT

"ospcustom" return true if an OSP record to the current car has been found
"neutral" return ascii code of neutral char (default 'n')
"reverse" return ascii code of reverse char (default 'r')
"sessiontype" see here: http://www.eksimracing.com/forum/index.php?topic=891.msg4560#msg4560
"ticks" return system tick count (int)
"speedmetric" return display unit true if imperial/MPH and false if metric/KMH
"simulation" return current simulation name (string) if available
"paused" return true if simulation is paused if available
"pluginactive" return 1 if a plugin has been activated
"pluginready" return true if plugin is ready to send data from simulation
"devicetype" return current targeted SLI USB device (integer - see mDeviceType global)
"tracksize" return current track size (meter) if available
"lapdistance" return the traveled distance of the car (meter) if available
"trackame" return current track name if available
"carname" return current car name if available
"position" return current car position if available
"laps" return laps completed (current lap) if available
"lapscount" return total lap (race) if available
"partialcount" or "sectorcount" number total of partial time
"blinktime" blinking delay (default 32)
"ospblinktime" blinking delay (default 8 )
"spdlmtrpmledonlyflag" rpm leds only when speedlimiter active? (boolean)
"noblinkflag" blinking allowed or not? (boolean)
"limitercharflag" limiter char must be showed? (boolean)
"ledspeedlimiterindex" return the current speedlimiter index (position) led (default 4 of value from 1 to 6)
"displaylaptime" lap time need to be showed or not (boolean)
"quickinfobutton" quick-info button is pressed or not (boolean)
"quickinfoleft" quick-info left panel index (position) function (integer)
"quickinforight" quick-info right panel index (position) function (integer)
"osptracking" osp tracking button activated or not (boolean)
"ospfactor" current osp factor (float)
"ospwithfirstgear" osp at first gear allowed (boolean)
"osplimitrpm" OSP rpm limit, result from my own OSP curve (integer)
"ospled1" OSP led 1 index (position) (integer)
"ospled2"  OSP led 2 index (position) (integer)
"yellowflag" return status of flags
"greenflag"
"redflag"
"blueflag"
"lastgearnorpm" return true if no shiftlights (RPM) on last gear
"rightdigits" return current position of right panel (int)
"leftdigits" return current position of left panel (int)
"globalbrightness" return current global brightness value
"lowfuel" return low fuel status
"dumplastlap" return status of telemetry report (true = activated)

-----------------------------------------------------------------------------------
GetCarInfo("selector")
returns car info (internal function)
Param "selector" (string), this string contains the following value:

IMPORTANT ** THESE VALUES ARE NOT AVAILABLE IN ALL SIMULATIONS ** IMPORTANT
"kers" return kers value
"kers max" return max kers energy
"drs" return ON or OFF status
"brakebiais" return current brake biais status
"rawspeed" return current car speed (meter/sec)
"speed" return current car speed in kph or mph depending the current display unit (metric or imperial)
"gear" return current car gear (-1 reverse; 0 = neutral; 1 = gear1,...)
"maxgear" return the current gear max of engine
"inpits" car is in pitlane? (integer)  0= on track; 1= pit stall area; 2= Approaching Pits (Pit Road)
"rpm" return current car rpm (r/mn)
"redzone" return current max engine rpm
"fuel" return current fuel in tank
"fuelmax" return the max capacity of fuel tank
"fuelperlap" return the consumption of fuel during the previous lap, updated after each lap
"fuelatstart" return the fuel in tank at the beginning of each lap
"watertemp" return water engine temp (celsius)
"oiltemp" return oil engine temp (celsius)
"sector" return current sector (integer)
"abs" return abs level (integer)
"tractioncontrol" return TC level (integer)
"speedlimiter" return spd lmt status (integer)
"headlights"
"overheating"
"detachedparts" return true if car is damaged

"wheeltempfrontleft", return front left average temp (celsius)
"wheeltempfrontright", return front right average temp (celsius)
"wheeltemprearleft", return rear left average temp (celsius)
"wheeltemprearright", return rear right average temp (celsius)

"wheelpressfrontleft", return front left average pressure (kpa)
"wheelpressfrontright", return front right average pressure (kpa)
"wheelpressrearleft", return rear left average pressure (kpa)
"wheelpressrearright", return rear right pressure (kpa)

"braketempfrontleft", return front left temp (celsius)
"braketempfrontright", return front right temp (celsius)
"braketemprearleft", return rear left temp (celsius)
"braketemprearright", return rear right temp (celsius)

-----------------------------------------------------------------------------------
GetTimeInfo("selector")
returns time info (internal function)
Param "selector" (string), this string contains the following value:
return float value

IMPORTANT ** THESE VALUES ARE NOT AVAILABLE IN ALL SIMULATIONS ** IMPORTANT

"laptime" return current lap time if available
"bestlaptime" return best lap time if available
"lastlaptime" return last lap time if available
"realdiffbest" return  real-time diff (delta) vs best  if available
"realdifflast" return real-time diff (delta) vs last  if available
"sector1" return current sector 1 if available
"sector2" return current sector 2 if available
"bestsector1" return best sector 1 if available
"bestsector2" return best sector 2 if available
"lastsector1" return last sector 1 if available
"lastsector2" return last sector 2 if available
"timeremaining" return session time remaining if available
"systemtime" return current PC time if available

-----------------------------------------------------------------------------------
GetPartialTimeInfo("selector", index)
returns partial time info
(internal function)
Param index (number) contains the targeted sector
Param "selector" (string), contains the following value:
return float value
see GetContextInfo() to get the total number of partials

IMPORTANT ** THESE VALUES ARE NOT AVAILABLE IN ALL SIMULATIONS ** IMPORTANT

"currentpartial" return current partial time if available
"bestpartial" return best partial time of this sector if available
"optimalpartial" return optimal partial time of this sector if available
"sessionbest" return session best partial time of this sector
"sessionoptimal" return session optimal partial time of this sector
"diffbestpartial" return diff from best if available
"diffoptimalpartial" return diff from optimal if available
"diffsessionbest" return diff from session best if available
"diffsessionoptimal"  return diff from session optimal if available

-----------------------------------------------------------------------------------
IsSLIFunction("selector", function_index)
returns boolean
(internal function)
return boolean value, true if function name matches the function index
Param index (number) contains the targeted function index returned by controlsEvent()
Param "selector" (string), contains the following value:
see controls.lua scripts for more info.

"brightness"
"quickinfo"
"speedmetric"
"lastgearnorpm"
"osptracking"
"safeprefs"
"laptimedelay"
"blinktime"
"lowfuel"
"speedlimitermethod"
"shiftlightsmethod"
"ospmethod"
"rightdigits"
"leftdigits"
"ospfactor"
"drivingsetup"
"maxgear"

===============================
 Script Developpment
 globals and utilities
===============================

Script Warriors do not reinvent the wheel and keep your script clean, optimized and obviously reuse the standard behaviors if possible ... ;)

-------------
 GLOBALS
-------------
- A great optimization of OSP and Shiftlights scripts with 2 new globals has been added.
gOSPLimit = limit/treshold computed by SLIMax Manager, the default value is set in gear.lua script. This value can be overwritten in your custom script, this way it lets you run the std OSP function (osp.lua) but with your own OSP curve. each gear of each car can have its own values.

gRedZone = the default value is set in gear.lua script. This value can be overwritten in your custom script, this way it let's you run the std Shiftlights function but with your own red zone value. each gear of each car can have its own values.

mLeftSpdLmtText and mRightSpdLmtText contains the default text for left and right digits panel in speedlimiter method 4 and 5 and can be "patched" (replaced with your own text) in custom_spdLmtMethodEvent() function (see custom_scripts.lua file)

mOSPMethod to overwrite the method value, useful in custom_ospMethodEvent() function to patch the current OSP method with another one

mSpdLimitMethod to overwrite the method value, useful in custom_spdLmtMethodEvent() function to patch the current PIT LIMITER method with another one

-----------------------
 SHIFTLIGHTS UTILITIES
-----------------------
[NEW] shiftlights utilities functions in SLIMax Mananger II script API 
(see shiftlights.lua for example and usage)

--------------------------------------------------------
shiftlights utilities functions for SLI-M and SLI-PRO
--------------------------------------------------------
-- shiftlights method from side to center
function SideToCenterSLI(rpm, redzone, p1, p2, p3, p4 ,p5 ,p6 ,p7)

-- progressive method
function ProgressiveSLI(rpm, redzone, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 )

-- alternate green, red and blue method
function AlternateSLI(rpm, redzone, step1, step2, step3)

-- percentage method
function percentageSLI(rpm, redzone, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 )

-- fixed rpm method, set rpm for each led (NOT RECOMMENDED)
function RpmSLI(rpm, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 )

-- progressive method but with rpm value for eah led
function ProgressiveFixedSLI(rpm, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13)

--------------------------------------------------------
shiftlights utilities functions for BU0710
--------------------------------------------------------
-- progressive method for BU0710
function ProgressiveBU0710(rpm, redzone, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 )

-- percentage method for BU0710
function percentageBU0710(rpm, redzone, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 )

-- fixed rpm method for BU0710 (NOT RECOMMENDED)
function RpmSLI(rpm, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 )

--------------------------------------------------------
LEFT and RIGHT Functions utilities
--------------------------------------------------------

-- add a function name to left and right functions list
-- typical usage FuncAddName(path, func): 
-- on left list:
-- local err = FuncAddName("cfg/sli_left_functions.ecfg", "100.DSII KERS")
-- or right list:
-- local err = FuncAddName("cfg/sli_right_functions.ecfg", "100.DSII KERS")
-- return 1 if ok


-- same as above for removing the function name
-- typical usage RemoveFuncName(path, func)
-- return 1 if ok

-- get the index of function 
-- typical usage: local idx = FuncGetPrefixIndex("100.DSII KERS")
-- return value > 0 if ok

-- get next function index available
-- typical usage FuncGetNewIndex(path): 
-- on left list:
-- local num_available = FuncGetNewIndex("cfg/sli_left_functions.ecfg")
-- or right list:
-- local num_available = FuncGetNewIndex("cfg/sli_right_functions.ecfg")
-- return value >=100 (0 to 99 are reserved )