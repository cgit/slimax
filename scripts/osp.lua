-- SLIMax Mgr Lua Script v2.1
-- Copyright (c)2012-2013  by Zappadoc - All Rights Reserved.
-- Use this script to build all Optimal ShiftPoints methods (OSP)
-- last change by Zappadoc - 2012-12-17

function SetOSPFeedback(state)
	if mOSPLED1 > 6 then
		SetExtLed( (mOSPLED1 - 6), state)
	else
		SetWarnLed(mOSPLED1, state)
	end
	if mOSPLED2 > 6 then
		SetExtLed( (mOSPLED2 - 6), state)
	else
		SetWarnLed(mOSPLED2, state)
	end
end

-- SLI-M, SLI-PRO OSP Methods Event
function ospMethodEvent(idx)
	mOSPMethod = idx
	-- OSP Limit default value is set in gear.lua
	if gOSPLimit == nil or gOSPLimit == 0 then gOSPLimit = GetContextInfo("osplimitrpm") end
	
	-- call custom script (this is a good place to overwrite gOSPLimit value)
	local result = custom_ospMethodEvent(mOSPMethod)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue

	-- call global custom script (this is a good place to overwrite gOSPLimit value)
	result = global_custom_ospMethodEvent(mOSPMethod)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- check if globals initialized
	if isGlobalInitialized == 0 then
		isGlobalInitialized = 1
		-- init
		InitGlobals()
	end
	-- get global prefs
	GetSLIMaxInfo()
	
	local led = ""
	
	-- get rpm
	local rpm = 0
	rpm = GetCarInfo("rpm")
	if rpm == nil then rpm = 0 end
	
	-- get red zone value
	--local redZone = GetCarInfo("redzone")
	--if redZone == nil then  redZone = 18000 end
	
	-- get current osp factor
	--local ospFactor = GetContextInfo("ospfactor")
	--if ospFactor == nil then  ospFactor = 80 end
	
	-- get current gear
	local gear = 0
	gear = GetCarInfo("gear")
	if gear == nil then  gear = 0 end
	
	-- get max gear of current car
	-- Alternate way to calc max gear if not available in API
	-- Use a global mMaxGear and do the following
	-- if gear > mMaxGear then mMaxGear = gear end	local maxGear = 4
	
	maxGear = GetCarInfo("maxgear")
	if maxGear == nil then  maxGear = 4 end
	
	-- skip if neutral or ==maxgear 
	if gear <=0 or gear>=maxGear then 
		-- do nothing
		return 1
	end 
	
	-- skip if first gear not allowed
	if not mOSPWithFirstGear and gear == 1 then
		-- do nothing
		return 1
	end
	
	-- print("OSP1: " .. mOSPLED1 .. " OSP2: " .. mOSPLED2 .."\n")
	-- print(rpm .. " - " .. gOSPLimit)
	
	-- rpm > osplimit so activate shiftpoints leds
	if rpm > gOSPLimit then

		-- no blinking allowed
		if mNoBlink then
			-- set both leds
			SetOSPFeedback(1)
			
		else
		
			if GetTicks() > mOldOSPTickCount then
				mOSPBlink = mOSPBlink + 1
			end
			
			if mOSPMethod <= 3 then
				-- methods 1 to 3
				if mOSPBlink >= mOSPBlinkTime then
					mOSPBlink = 0
				end

				if mOSPBlink <= (mOSPBlinkTime / 2) then
					if mOSPMethod ~= 3 then
						-- if not method 3
						SetOSPFeedback(1)
					end
					if (mOSPMethod == 1) or (mOSPMethod == 3) then
						-- add blue leds blinking if method 1 or 3
						for i =  9,12 do 
							led = led.format("RPM%d",i)
							mRPMLedTable[led] = 1
						end	
						SetRPMLed("mRPMLedTable")
					end
				end

				if mOSPBlink > (mOSPBlinkTime / 2) then
					SetOSPFeedback(0)
					
					if (mOSPMethod == 1) or (mOSPMethod == 3) then
						-- add blue leds blinking if method 1 or 3
						for i =  9,12 do 
							led = led.format("RPM%d",i)
							mRPMLedTable[led] = 0
						end	
						SetRPMLed("mRPMLedTable")
					end
				end
			end
			
			if mOSPMethod == 2 then
				-- add blue leds not blinking if method 2
				for i =  9,12 do 
					led = led.format("RPM%d",i)
					mRPMLedTable[led] = 1
				end	
				SetRPMLed("mRPMLedTable")
			end
		end
	else
		-- bypass (give control to manager)
		return 0
	end
	-- timebase
	if GetTicks() > mOldOSPTickCount then
		mOldOSPTickCount = GetTicks() + 10
	end
	
	return 1
end