-- SLIMax Mgr Lua Script v2
-- Copyright (c)2011-2013 by EK and Zappadoc - All Rights Reserved.
-- Use this script to bypass any previous functions and show what 
-- you want onto your device.


-- param = device type (integer - see mDeviceType table)
-- return 1 to send processed data to the device and bypass Mgr
-- return 0 to skip and give the control to Mgr
function deviceReport(devType)
	-- call custom script
	local result = custom_deviceReport(devType)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue

	-- call global custom script
	result = global_custom_deviceReport(devType)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue

	
	-- sim paused?
	local paused = GetContextInfo("paused")
	-- limiter ON?
	local spdLmt = GetCarInfo("speedlimiter")
	if paused or spdLmt == 0 then
		-- reset value
		SetDigitsAllowed(true)
	end
	
	
	-- skip and give the control to Mgr
	return 0

end

