-- iRacing SLI-PRO Custom SLIMax Manager Scripts v2.3
-- Copyright �2012-2013 by Zappadoc - All Rights Reserved.
-- last change by Zappadoc - 2012-12-17

-- ================================
-- CONSTANTS


-- ================================
-- additional lua extension module dll


-- ================================
-- additional scripts file
require "scripts/zdoc_scripts/iracing_stuff"

-- ================================
-- custom globals


-- ================================
-- custom functions

-- ================================
-- custom events

function custom_initEvent(scriptfile)
	-- type your custom script initialization here
end

function custom_controlsEvent(deviceIdx, ctrlType, ctrlPos, value, funcIndex)
	-- type your custom controls script here (manage buttons, switches and encoders)
	return 2
end

function custom_deviceReport(devType)
	-- type your script here (just before sending report to the device )
	return 2
end

function custom_ospMethodEvent(idx)
	-- type your custom Optimal Shift Points (OSP) method here
		-- custom speedlimiter for each iRacing car
	local result = 2
	-- get current simulation name
	local sim = GetContextInfo("simulation")
	if isAppIRacing(sim) then
		result = iRacing_ospMethodEvent(idx)
		
	end
	return result
end

function custom_shiftLightsMethodEvent(idx)
	-- type your custom shiftlights method here
	
	-- custom shiftlights for each iRacing car
	-- get current simulation name
	local sim = GetContextInfo("simulation")
	if isAppIRacing(sim) then
		iRacing_shiftLightsMethodEvent(idx)
		-- skip std methods
		return 1
	end
	
	return 2
end

function custom_shiftLightsBU0710Event(idx)
	-- type your custom shiftlights method for BU0710 device only here
	return 2
end

function custom_leftDigitsEvent(swPosition)
	-- type your custom script related to left SLI-PRO digits panel here
	return 2
end

function custom_rightDigitsEvent(swPosition)
	-- type your custom script related to right SLI-PRO digits panel here
	return 2
end

function custom_spdLmtMethodEvent(idx)
	-- type your custom speedlimiter method here
		
	-- custom speedlimiter for each iRacing car
	local result = 2
	-- get current simulation name
	local sim = GetContextInfo("simulation")
	if isAppIRacing(sim) then
		result = iRacing_spdLmtMethodEvent(idx)
		
	end
	return result
end

function custom_gearEvent(gear)
	-- type your custom gear event script here
		-- get current simulation name
	-- custom script to overwrite OSP value and OSP limit for each gear of each iRacing car
	local sim = GetContextInfo("simulation")
	if isAppIRacing(sim) then	
		if gOldGear == nil then gOldGear = -2 end
		-- setup iRacing car OSP, RedZone, Max gear, ...
		iRacing_CarSetup(gear)
		if gear ~= gOldGear then
			gOldGear =  gear
			-- print("\n----------\nGear: ".. gear .."\nOSP: " .. gOSPLimit .. "\nRedZone:" .. gRedZone .."\n")
		end
	end
	return 2
end

function custom_enterSessionEvent(devType)
	-- type your custom script on session start, here

	-- custom script to get the max gear of each iRacing car
	-- get current simulation name
	local sim = GetContextInfo("simulation")
	if isAppIRacing(sim ) then
		iRacing_CarSetup(0)
		-- see debug window
		-- get car name
		local cNm = GetContextInfo("carname")
		local mxG = GetCarInfo("maxgear")
		local ospf = GetContextInfo("ospfactor")
		print("\n----------\nSetup car:" .. cNm .. "\nMaxGear:" .. mxG .. "\nOSP Factor:" .. ospf .. "\n");
	end
	return 2
end

function custom_exitSessionEvent(devType)
	-- type your custom script on session ending, here
	return 2
end