-- Custom SLIMax Manager Scripts for iRacing v2.1
-- Copyright �2012-2013 by Zappadoc
-- _last change by Zappadoc - 2012-12-17

-- =====================================
-- iRacing Stuff script
-- define car setup function and all pit limiter and shiftlights methods for each car you drive


function iRacing_CarSetup(cGear)
	-- get car name
	local cNm = ""
	cNm = GetContextInfo("carname")
	local mxG = 4
	local ospf = 140
	
	if cNm == "cadillacctsvr" then
		mxG = 6
		ospf = 148
		
	elseif cNm == "mclarenmp4" then
		mxG = 6
		ospf = 150

	elseif cNm == "formulamazda" then
		mxG = 6
		ospf = 60
		
	elseif cNm == "williamsfw31" then
		mxG = 7
		ospf = 90
		
	elseif cNm == "mx5 cup" or cNm == "mx5 roadster" then
		mxG = 6
		ospf = 140
		
	elseif cNm == "hpdarx01c" then
		mxG = 6
		if cGear <= 2 then 
			ospf = 10
			gOSPLimit = 9900
			gRedZone = 9920
		elseif cGear ==3 then
			ospf = 100
			gOSPLimit = 9530
			gRedZone = 9545
		elseif cGear == 4 then 
			ospf = 90
			gOSPLimit = 9500
			gRedZone = 9510
		else 
			ospf = 150
			gOSPLimit = 9420
			gRedZone = 9425
		end
		

	elseif cNm == "fordgt" then
		mxG = 6
		ospf = 128
		
	elseif cNm == "fr500s" then
		mxG = 6
		ospf = 150
				
	elseif cNm == "fordv8sc" then
		mxG = 6
		ospf = 100
		
	 elseif cNm == "dallara" then
		mxG = 6
		ospf = 95
		
	elseif cNm == "latemodel" then
		mxG = 5
		
	elseif cNm == "legends ford34c" then
		mxG = 5
		ospf = 150
		
	elseif cNm == "skmodified" then
		mxG = 6
		
	elseif cNm == "silvercrown" then
		mxG = 6
		
	elseif cNm == "trucks silverado" then
		mxG = 4
		
	elseif cNm == "stockcars impala" then
		mxG = 4
		ospf = 140
		
	elseif cNm == "lotus79" then
		mxG = 5
		ospf = 100
		
	elseif cNm == "stockcars2 chevy" then
		mxG = 4
		ospf = 130
		
	elseif cNm == "stockcars2 chevy cot" then
		mxG = 4
		ospf = 150
		
	elseif cNm == "c6r" then
		mxG = 6
		ospf = 145
		
	elseif cNm == "solstice" then
		mxG = 5
		ospf = 150
		
	elseif cNm == "jettatdi" then
		mxG = 5
		
	elseif cNm == "radical sr8" then
		mxG = 6
		ospf = 115
				
	elseif cNm == "rt2000" then
		mxG = 5
		ospf = 140
		
	elseif cNm == "specracer" then
		mxG = 5
		ospf = 140
		
	elseif cNm == "rileydp" then
		mxG = 5
		ospf = 85
		
	elseif cNm == "streetstock" then
		mxG = 4
		ospf = 150
		
	elseif cNm == "sprint" then
		mxG = 6
		
	else
		-- default 
		mxG = 4
		ospf = 150
	end
	-- set max gear and OSP factor for this car
	SetMaxGear(mxG)
	SetOSPFactor(ospf)
end

function FixRPMSpeedlimiter()
	local led = ""
	-- init table
	initLedTable(mRPMLedTable, 0)
	SetRPMLed("mRPMLedTable")
	for i = 0,12 do 
		led = led.format("RPM%d",i)
		mRPMLedTable[led] = 1
	end
	SetRPMLed("mRPMLedTable")
		
end

function iRacing_spdLmtMethodEvent(idx)
	local led = ""
	
	-- get car name
	local cNm = GetContextInfo("carname")
	
	-- speedlimiter stuff
	if cNm == "formulamazda" then
		mSpdLimitMethod = 2

	elseif cNm == "mclarenmp4" then
		mSpdLimitMethod = 2
		
	elseif cNm == "williamsfw31" then
		-- speedlimiter led fixed + digits blinking, method 6
		mSpdLimitMethod = 6
		
	elseif cNm == "fordv8sc" then
		mSpdLimitMethod = 2
		
	elseif cNm == "dallara" then
		mSpdLimitMethod = 2
	
	elseif cNm == "fordgt" then
		mSpdLimitMethod = 2
		
 	elseif cNm == "hpdarx01c" then
		FixRPMSpeedlimiter()
		return 1
		
	elseif cNm == "rileydp" then
		mSpdLimitMethod = 2
	
	elseif cNm == "c6r" then	
		FixRPMSpeedlimiter()
		return 1

	elseif cNm == "radical sr8" then
		mSpdLimitMethod = 0
	
	elseif cNm == "jettatdi" then
		mSpdLimitMethod = 2
	
	elseif cNm == "cadillacctsvr" then
		FixRPMSpeedlimiter()
		return 1
		
	-- cars with  without dashboardleds
	-- 	elseif cNm == "mx5 cup" or cNm == "mx5 roadster" then
	-- elseif cNm == "fr500s" then
	-- elseif cNm == "latemodel" then
	-- elseif cNm == "legends ford34c" then
	-- elseif cNm == "legends ford34c rookie" then
	-- elseif cNm == "skmodified" then
	-- elseif cNm == "silvercrown" then
	-- elseif cNm == "trucks silverado" then
	-- elseif cNm == "stockcars impala" then
	-- elseif cNm == "lotus79" then
	-- elseif cNm == "stockcars2 chevy" then
	-- elseif cNm == "solstice" then
	-- elseif cNm == "solstice rookie" then
	-- elseif cNm == "rt2000" then
	-- elseif cNm == "specracer" then
	-- elseif cNm == "streetstock" then
	-- elseif cNm == "sprint" then
	
	else
		-- default 
		mSpdLimitMethod = 0
		
	end
	return 2
end

function iRacing_shiftLightsMethodEvent(idx)
	-- rpm table name
	local tName = "mRPMLedTable"
	-- get rpm
	local rpm = 0
	rpm = GetCarInfo("rpm")
	if rpm == nil then rpm = 0 end
	
	-- check redzone value
	if gRedZone == nil or gRedZone == 0 then gRedZone = GetCarInfo("redzone") end

	-- get car name
	local cNm = GetContextInfo("carname")
	
	local isPit = GetCarInfo("inpits")
	local carspd =  GetCarInfo("rawspeed")
	
	-- init leds (see global.lua)
	initLedTable(mRPMLedTable, 0)
	
	-- Pits Stuff
	if isPit == 1 and carspd == 0 then
		--  pit stop
		
	elseif isPit >= 1 and carspd > 21 then
		-- car Approaching pits (or in pit lane) and exceed 45 mph
		-- set the red zone to the current engine rpm (This sets the shiftlights to the max and warns the driver)
		local spdLmt = GetCarInfo("speedlimiter")
		if spdLmt == 0 then
			-- if speedlimiter is OFF
			gRedZone = rpm
		end
	end
	
	-- shiftlights stuff
	if cNm == "formulamazda" then
		-- start Mazda shiftlights
		SideToCenterSLI(rpm, gRedZone, 94.5, 95, 96, 97 ,98 ,99 ,99.5)
	
	elseif cNm == "mclarenmp4" then
		local gear = GetCarInfo("gear")
		if gear == 6 then
			ProgressiveFixedSLI(rpm, 6300, 6490, 6580, 6690, 6720 , 6870 , 7000 , 7200, 7305, 7340, 7340, 7340, 7340)		
		else
			ProgressiveFixedSLI(rpm, 6200, 6390, 6480, 6590, 6620 , 6670 , 6750 , 6800, 6895, 7040, 7040, 7040, 7040)
		end
		
	elseif cNm == "williamsfw31" then
		AlternateSLI(rpm, gRedZone, 11.0, 12.0, 12.9)
		
	elseif cNm == "mx5 cup" or cNm == "mx5 roadster" then
		SideToCenterSLI(rpm, gRedZone, 86, 90, 95, 96 ,97 ,98 ,99)
		
	elseif cNm == "fr500s" then
		SideToCenterSLI(rpm, gRedZone, 80, 84, 86, 88 ,90 ,98 ,99)
		
	elseif cNm == "fordv8sc" then
		SideToCenterSLI(rpm, gRedZone, 87, 90, 92, 94 ,97 ,98 ,99)
		
	 elseif cNm == "dallara" then
		SideToCenterSLI(rpm, gRedZone, 78, 82, 90, 94 ,97 ,98 ,99)
-- 		
	elseif cNm == "fordgt" then
		SideToCenterSLI(rpm, gRedZone, 93, 94, 95, 96 ,97 ,98 ,99)
		
 	elseif cNm == "hpdarx01c" then
		ProgressiveFixedSLI(rpm, 7800, 7900, 8100, 8300, 8500 , 8700 , 8800 , 8900, 9100, 9300, 9500, 9700, 9940)
	
	elseif cNm == "rileydp" then
		ProgressiveFixedSLI(rpm, 10000, 6700, 6800, 6900, 7000 , 10000 , 10000 , 10000, 10000, 10000, 10000, 10000, 10000)
	
	elseif cNm == "c6r" then	
		ProgressiveFixedSLI(rpm, 4700, 4800, 4900, 5000, 5200 , 5300 , 5400 , 5600, 5700, 5800, 5900, 6000, 6100)

	elseif cNm == "radical sr8" then
		ProgressiveFixedSLI(rpm, 4350, 4800, 5100, 5700, 6400 , 6900 , 7700 , 8100, 8700, 9850, 10100, 10250, 10500)
	
	elseif cNm == "jettatdi" then
		SideToCenterSLI(rpm, gRedZone, 80, 84, 86, 88 ,90 ,98 ,99)
	
	elseif cNm == "cadillacctsvr" then
		--ProgressiveFixedSLI(rpm, 6500, 6580, 6600, 6700, 6800 , 6900 , 7000 , 7100, 7200, 7300, 7400, 7500, 7600)
		ProgressiveSLI(rpm, gRedZone, 11.9, 11.95, 11.98, 12, 12.1, 12.15, 12.2, 12.25, 12.3, 12.35, 12.5, 12.8, 12.98 )

	-- progressive default method for all cars with dashboard without leds 
	-- elseif cNm == "latemodel" then
	-- elseif cNm == "legends ford34c" then
	-- elseif cNm == "legends ford34c rookie" then
	-- elseif cNm == "skmodified" then
	-- elseif cNm == "silvercrown" then
	-- elseif cNm == "trucks silverado" then
	-- elseif cNm == "stockcars impala" then
	-- elseif cNm == "lotus79" then
	-- elseif cNm == "stockcars2 chevy" then
	-- elseif cNm == "solstice" then
	-- elseif cNm == "solstice rookie" then
	-- elseif cNm == "rt2000" then
	-- elseif cNm == "specracer" then
	-- elseif cNm == "streetstock" then
	-- elseif cNm == "sprint" then
	
	else
		-- default 
		ProgressiveSLI(rpm, gRedZone, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 12.8, 12.98 )
	end
		
	-- set leds
	SetRPMLed(tName)	
end

function iRacing_ospMethodEvent(idx)
	-- get car name
	local cNm = GetContextInfo("carname")
	-- OSP Method stuff
	if cNm == "mclarenmp4" or cNm == "williamsfw31" then
		if mOSPMethod ~= nil then  mOSPMethod = 3 end
	end
	return 2
end