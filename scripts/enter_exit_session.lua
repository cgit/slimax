-- SLIMax Mgr Lua Script v2
-- Copyright (c)2011-2013 by EK and Zappadoc - All Rights Reserved.
-- Event received when the car enter and exit the session.

-- param = device type (integer - see mDeviceType table)
-- enter session
function enterSessionEvent(devType)
	-- call custom script
	local result = custom_enterSessionEvent(devType)
	if result <= 1 then return result end
	-- if result >= 2 continue

	-- call global custom script
	result = global_custom_enterSessionEvent(devType)
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- do nothing
	return 1
end

-- param = device type (integer - see mDeviceType table)
-- exit session
function exitSessionEvent(devType)
	-- call custom script
	local result = custom_exitSessionEvent(devType)
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- call global custom script
	result = global_custom_exitSessionEvent(devType)
	if result <= 1 then return result end
	-- if result >= 2 continue

	-- do nothing
	return 1
end
