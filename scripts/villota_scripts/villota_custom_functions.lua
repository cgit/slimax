-- Custom Scripts Template SLIMax Manager Scripts v2.0
-- Copyright �2012-2013 by Zappadoc - All Rights Reserved.
-- last change by Zappadoc - 2012-12-03


-- villota77's script for F1 2012 - v1.3.2

-- This script tries to imitate the way KERS and delta time info is shown in real F1 steering wheel displays
-- (1) KERS is shown in the left display as a pure number (in percentage)
-- (2) Delta time ("qualy time") with respect to best (or last) lap is shown in the right display
--     in  X.XX format (if it is positive - note there is not a "+" sign in SLIPro)
--     or -X.XX format (if it is negative)
-- (3) Delta-time info is refreshed every 1 second so you don't have digits flickering
-- (4) At the end of the lap, the right display will freeze and show deltatime (to best lap) during a number of seconds chosen in SLIMaxMng
---	   (General Options - LAPTIMEDISPLAYDELAY). Then it will resume showing real-time data.
 
 
function ev_ShowTime(lptime, T, Ndig, deltaflag)
   local ticks = GetTicks()
   -- show time every T milliseconds with Ndig decimal digits

   if ev_old_time == nil  or (ticks - ev_old_time) > T then
    -- global to backup ticks (ms)
		ev_old_time = ticks
 
		local hr = 0
		local mn = 0
		local sc = 0
		local ms = 0
		local hd = 0
		local rpanel = ""
		local prefix = " "
		
		if lptime < 0 then
			prefix = "-"
		end
		-- explod time
		hr, mn, sc, hd, ms = timeDispatcher(lptime)
			
		if deltaflag == 1 then
			--display delta time
			if lptime == -1 or (mn + sc + ms) == 0.0 then
				rPanel =  "  -.-- "
			elseif mn > 0 then
				rPanel =  string.format( " %s%1d.%02d ", prefix, mn, sc)
			else
				if Ndig == 2 then
					rPanel =  string.format( " %s%1d.%02d ", prefix, sc, hd)
				else
					rPanel =  string.format( " %s%1d.%03d", prefix, sc, ms)
				end
			end
			
		elseif deltaflag == 0 then
			--display laptime
			if lptime == -1 or (mn + sc + ms) == 0.0 then
				rPanel =  "-:--.---" 
			elseif mn < 10 then
				rPanel =  string.format( "%1d:%02d.%03d", mn, sc, ms)
			elseif hr > 0 then
				rPanel =  string.format( " %02d.%02d ", hr, mn)   
			else
				rPanel =  string.format( " %02d.%02d.%01d", mn, sc, ms)
			end
		end
	end
	
	SetRightDigits( rPanel )
	-- return 1 to bypass std behavior
	return 1
end


function villota_custom_leftDigitsEvent(swFunction)

	-- get current simulation name
	local sim = GetContextInfo("simulation")
	
	if sim == "f1_2012.exe" then
	
		local lPanel = ""
		
		-- is OSP Tracking ON
		local ospt = GetContextInfo("osptracking")
		if ospt then return 2 end
	
		-- check if quick info button is down
		local qi = GetContextInfo("quickinfobutton")
		if qi then return 2 end	
	
		if swFunction == 33 then
			-- 33: KERSpercentage, wothout prefix
			local kersp = GetCarInfo("kers")
			lPanel = string.format(" %3d  ", round((kersp/1000)/4))
			SetLeftDigits( lPanel ) 
			return 1

		end

	end	
	return 2
end

function villota_custom_rightDigitsEvent(swFunction)

	-- get current simulation name
	local sim = GetContextInfo("simulation")
	if sim == "f1_2012.exe" or sim == "rFactor.exe" or sim == "rFactor2.exe" or sim == "LFS.exe" or sim == "GTR2.exe" then

		local delta = 0.0

		-- is OSP Tracking ON
		local ospt = GetContextInfo("osptracking")
		if ospt then return 2 end
			
		-- check if quick info button is down
		local qi = GetContextInfo("quickinfobutton")
		if qi then return 2 end	
			
		local dltime = GetContextInfo( "displaylaptime" )
		if dltime == true then
			--freeze last delta time in display
			local lpt_last = GetTimeInfo("lastlaptime")
			local lpt_best = GetTimeInfo("bestlaptime")
			if ev_old_bestLapTime == nil then ev_old_bestLapTime = lpt_best end			
			ev_final_delta_best = lpt_last - ev_old_bestLapTime
			if ev_final_delta_best == 0 then
				return 2
			else
				return ev_ShowTime(ev_final_delta_best, 50, 3, 1)
			end
		else
			--update best time if last laptime was better
			if ev_final_delta_best ~= nil and ev_final_delta_best < 0 then
				ev_old_bestLapTime = lpt_last 
			end
		end
		
		if swFunction == 11 then
			-- 11: real time best laptime difference DELTA time
			-- real time diff vs your best
          
			delta = GetTimeInfo("realdiffbest")
			return ev_ShowTime(delta, 1000, 2, 1)
		
		elseif swFunction == 12 then
			-- 12: real time last laptime difference DELTA time
			-- real time diff vs your last
          
			delta = GetTimeInfo("realdifflast")
			return ev_ShowTime(delta, 1000, 2, 1)
		end

	end
	return 2
end


