-- SLIMax Mgr Lua Script v2
-- Copyright (c)2011-2013 by EK and Zappadoc - All Rights Reserved.
-- gear event
-- last change by Zappadoc - 2012-04-08

-- SLI-M, SLI-PRO Gear Event
function gearEvent(gear)
	-- get the rpm limit already computed by my OSP method
	-- using OSP Factor parameter
	-- value used in OSP, Shiftlights, car setup,... and can be overwritten
	gOSPLimit = 0
	gOSPLimit = GetContextInfo("osplimitrpm")
	if gOSPLimit == nil then gOSPLimit = 0 end
	
	-- get the red zone already computed by my SLIMax Mgr II
	-- value used in Shiftlights and can be overwritten
	gRedZone = 0
	gRedZone = GetCarInfo("redzone") 
	if gRedZone == nil then gRedZone = 18000 end
	
	-- call custom script
	local result = custom_gearEvent(gear)
	if result <= 1 then return result end
	
	-- call global custom script
	result = global_custom_gearEvent(gear)
	if result <= 1 then return result end

	local g = gear
	if oldGear == nil then oldGear = GetContextInfo("neutral") end
	
	-- get neutral 
	local n = GetContextInfo("neutral")
	-- get reverse char and convert to string
	local r = string.char(GetContextInfo("reverse"))
	-- get state of custom Optimal Shift Point (OSP) records
	local ospcustom_on = GetContextInfo("ospcustom")

	-- optimize display, call if changed
	if oldGear ~= g or g == 0 then
		-- set neutral, reverse or current gear
		if g == 0 then 
			-- if neutral and using custom OSP record then add a dot to gear digit
			if ospcustom_on  then n = n + 128 end
			SetGearDigit(string.char(n))
		elseif g < 0 then 
			SetGearDigit(r)
		else 
			-- if gear = 'E' give the hand to SLIMax Manager II
			if g == 69 then
				return 0
			end

			SetGearDigit(string.char(g))
		end	
		


		-- print( string.format("Gear: %d\n", gear))
	end
	-- backup gear state
	oldGear = g
	
	return 1
end