-- Part of SLIMax Mgr II - Lua Script v2.2
-- Copyright (c)2012-2013  by Zappadoc - All Rights Reserved.
-- Use this script to patch std led behaviors (i.e lowfuel, flags, damage, rev limit, ...)
-- created by Zappadoc - 2012-11-10

-- SLI-M, SLI-PRO, SLI-F1 led event
-- params:
-- idx = led index (assigned in settings)
-- state = the std state of the led; activated if > 0
-- ledFunction: the corresponding function
-- 1=LowFuel
-- 2=TC
-- 3=ABS
-- 4=Green Flag
-- 5=Yellow flag
-- 6=Red Flag
-- 7=Over Heating
-- 8=Damage
-- 9=Pit request
-- 10=Power
-- 11=Rev Limit
-- 12=HeadLights
-- 13=Blue Flag
-- 14=DRS
-- 15=Safe Prefs

function ledEvent(idx, ledFunction, state)
	-- call custom script
	local result = custom_ledEvent(idx, ledFunction, state)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- call global custom script
	result = global_custom_ledEvent(idx, ledFunction, state)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- return 0 to use std behaviors
	return 0
end
 