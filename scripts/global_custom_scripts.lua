-- Global Custom SLIMax Manager Scripts v2.2
-- Copyright �2011-2013 by Zappadoc - All Rights Reserved.
-- last change by Zappadoc - 2012-11

-- add you global custom functions and globals variables here 
-- patch the std SLIMax Events with your global custom scripts if needed
-- see the scripting section of the forum for more info...

-- IMPORTANT:
-- this script will not be deleted by uninstalling the software

-- ================================
-- CONSTANTS


-- ================================
-- additional lua extension module dll


-- ================================
-- additional scripts file

-- ================================
-- custom globals


-- ================================
-- custom functions

-- ================================
-- custom events

function global_custom_controlsEvent(deviceIdx, ctrlType, ctrlPos, value, funcIndex)
	-- type your custom controls script here (manage buttons, switches and encoders)
	return 2
end

function global_custom_deviceReport(devType)
	-- type your script here (just before sending report to the device )
	return 2
end

function global_custom_ospMethodEvent(idx)
	-- type your custom Optimal Shift Points (OSP) method here
	return 2
end

function global_custom_shiftLightsMethodEvent(idx)
	-- type your custom shiftlights method here	
	return 2
end

function global_custom_shiftLightsBU0710Event(idx)
	-- type your custom shiftlights method for BU0710 device only here
	return 2
end

function global_custom_leftDigitsEvent(swPosition)
	-- type your custom script related to left SLI-PRO digits panel here
	return 2
end

function global_custom_rightDigitsEvent(swPosition)
	-- type your custom script related to right SLI-PRO digits panel here
	return 2
end

function global_custom_spdLmtMethodEvent(idx)
    if idx > 2 then
        -- reinit RPM leds only if method > 2
        initLedTable(mRPMLedTable, 0)
        SetRPMLed("mRPMLedTable")
    end

    -- continue with standard behaviour
	return 2
end

function global_custom_gearEvent(gear)
	-- type your custom gear event script here
    -- check if gear == 'E' (see ascii table )
    if gear == 69 then

      -- toggle OFF in this example
      toggleAllLed(0)   

      -- control gear digit display
      SetGearDigit(GetCurrentGear())

      -- control left panel (6 chars max)
      -- nothing display in this example
      local ldigits = "      "
      SetLeftDigits( ldigits )
      -- control right panel (6 chars max)
      -- nothing display in this example
      local rdigits = "      "
      SetRightDigits( rdigits )

      -- refresh SLI board
      SLISendReport(1)

      -- return 1 to bypass std behavior
      return 1
    end

	return 2
end

function global_custom_enterSessionEvent(devType)
    initLedTable(mRPMLedTable,0)
	SetRPMLed("mRPMLedTable")

    for j = 1, 0 do
        for i = 0, 12 do
            led = led.format("RPM%d",i)
            mRPMLedTable[led] = j
            SetRPMLed("mRPMLedTable")
            print( string.format("rpm%d : %d", i, j))
            win.Sleep(200)
        end
    end

	return 2
end

function global_custom_exitSessionEvent(devType)
	-- type your custom script on session ending, here
    local ldigits = " exit "
    SetLeftDigits( ldigits )
    local rdigits = " exit "
    SetRightDigits( rdigits )

    -- refresh SLI board
    SLISendReport(1)

	return 1
end

function global_custom_ledEvent(idx, ledFunction, state)
	return 2
end


-- ================================
-- local custom events PLACEHOLDERS
-- DO NOT CHANGE THE SCRIPT BELOW
-- ================================
function custom_controlsEvent(deviceIdx, ctrlType, ctrlPos, value, funcIndex) return 2 end
function custom_deviceReport(devType) return 2 end
function custom_ospMethodEvent(idx) return 2 end
function custom_shiftLightsMethodEvent(idx) return 2 end
function custom_shiftLightsBU0710Event(idx) return 2 end
function custom_leftDigitsEvent(swPosition) return 2 end
function custom_rightDigitsEvent(swPosition) return 2 end
function custom_spdLmtMethodEvent(idx) return 2 end
function custom_gearEvent(gear) return 2 end
function custom_enterSessionEvent(devType) return 2 end
function custom_exitSessionEvent(devType) return 2 end
function custom_ledEvent(idx, ledFunction, state) return 2 end
