-- Custom Scripts Template SLIMax Manager Scripts v2.0
-- Copyright �2012-2013  by Zappadoc - All Rights Reserved.
-- last change by Zappadoc - 2012-12-02


-- villota77's script for F1 2012 - v1.3.2

-- This script tries to imitate the way KERS and delta time info is shown in real F1 steering wheel displays
-- (1) KERS is shown in the left display as a pure number (in percentage)
-- (2) Delta time ("qualy time") with respect to best (or last) lap is shown in the right display
--     in  X.XX format (if it is positive - note there is not a "+" sign in SLIPro)
--     or -X.XX format (if it is negative)
-- (3) Delta-time info is refreshed every 1 second so you don't have digits flickering
-- (4) At the end of the lap, the right display will freeze and show deltatime (to best lap) during a number of seconds chosen in SLIMaxMng
---	   (General Options - LAPTIMEDISPLAYDELAY). Then it will resume showing real-time data.
 

-- ================================
-- CONSTANTS

-- ================================
-- additional lua extension module dll

-- ================================
-- additional scripts file
require "scripts/villota_scripts/villota_custom_functions"

-- ================================
-- custom globals

-- ================================
-- custom functions

-- ================================
-- custom events


function custom_initEvent(scriptfile)
	-- type your custom script initialization here
end

function custom_controlsEvent(deviceIdx, ctrlType, ctrlPos, value, funcIndex)
	-- type your script here (just before sending report to the device )
	return 2
end

function custom_deviceReport(devType)
	-- type your script here (just before sending report to the device )
	return 2
end

function custom_ospMethodEvent(idx)
	-- type your custom Optimal Shift Points (OSP) method here
	return 2
end

function custom_shiftLightsMethodEvent(idx)
	-- type your custom shiftlights method here
	return 2
end

function custom_shiftLightsBU0710Event(idx)
	-- type your custom shiftlights method for BU0710 device only here
	return 2
end

function custom_leftDigitsEvent(swFunction)
	-- call custom function
	return villota_custom_leftDigitsEvent(swFunction)
end

function custom_rightDigitsEvent(swFunction)
	-- call custom function
	return villota_custom_rightDigitsEvent(swFunction)
end


function custom_spdLmtMethodEvent(idx)
	-- type your custom speedlimiter method here
	return 2
end

function custom_gearEvent(gear)
	-- type your custom gear event script here
	return 2
end

function custom_enterSessionEvent(devType)
	-- type your custom script on session start, here
	return 2
end

function custom_exitSessionEvent(devType)
	-- type your custom script on session ending, here
	return 2
end

-- triggered from v2.2.4 of SMX2
function custom_ledEvent(ledIndex, ledFunction, ledState)
	-- type your custom script on session ending, here
	return 2
end
