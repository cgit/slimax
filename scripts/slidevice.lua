-- Load SLIMax Mgr Lua Scripts v2.1
-- Copyright (c)2011-2013 by Zappadoc - All Rights Reserved.
-- changed by Zappadoc - 2012-11-10
-- main device script

require "scripts/led"
require "scripts/enter_exit_session"
require "scripts/gear"
require "scripts/slipro"
require "scripts/shiftlights"
require "scripts/speedlimiter"
require "scripts/osp"
require "scripts/controls"
require "scripts/devhook"
require "scripts/global_custom_scripts"

-- load the custom scripts if exists
scr = GetCustomScripts("scriptname")
-- print ( scr )
if scr ~= nil and scr ~= "" then
	mCustomScriptsFileName = scr
	require(mCustomScriptsFileName)
end
