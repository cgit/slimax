-- SLIMax Mgr Lua Script v2.3
-- Copyright (c)2011-2013 by EK and Zappadoc - All Rights Reserved.
-- Use this script to build all shiftlights methods
-- last change by Zappadoc - 2012-10-28

-- ==============================================================================
-- utilities functions and methods
-- ==============================================================================
-- SLI-M and SLI-PRO functions

-- shiftlights method from side to center
function SideToCenterSLI(rpm, redzone, p1, p2, p3, p4 ,p5 ,p6 ,p7)

	local rz = redzone / 100
	
	-- side to center custom
	if rpm > (rz*p1) then mRPMLedTable.RPM0 = 1 end -- G
	if rpm > (rz*p1) then mRPMLedTable.RPM12 = 1 end -- B

	if rpm > (rz*p2) then mRPMLedTable.RPM1 = 1 end -- G
	if rpm > (rz*p2) then mRPMLedTable.RPM11 = 1 end -- B

	if rpm > (rz*p3)	then mRPMLedTable.RPM2 = 1 end -- G
	if rpm > (rz*p3)	then mRPMLedTable.RPM10 = 1 end -- B

	if rpm > (rz*p4) then mRPMLedTable.RPM3 = 1 end -- G
	if rpm > (rz*p4) then mRPMLedTable.RPM9 = 1 end -- B

	if rpm > (rz*p5) then mRPMLedTable.RPM8 = 1 end -- R
	if rpm > (rz*p5) then mRPMLedTable.RPM4 = 1 end -- R

	if rpm > (rz*p6)	then mRPMLedTable.RPM5 = 1 end -- R
	if rpm > (rz*p6)	then mRPMLedTable.RPM7 = 1 end -- R

	if rpm > (rz*p7)	then mRPMLedTable.RPM6 = 1 end -- R

end

-- progressive method
function ProgressiveSLI(rpm, redzone, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 )
	local rz = redzone / 13
	-- print("progessive")
	if rpm > (rz*p1) 	then mRPMLedTable.RPM0 = 1 end
	if rpm > (rz*p2) 	then mRPMLedTable.RPM1 = 1 end
	if rpm > (rz*p3)	then mRPMLedTable.RPM2 = 1 end
	if rpm > (rz*p4) 	then mRPMLedTable.RPM3 = 1 end

	if rpm > (rz*p5) 	then mRPMLedTable.RPM4 = 1 end
	if rpm > (rz*p6)  	then mRPMLedTable.RPM5 = 1 end
	if rpm > (rz*p7)	then mRPMLedTable.RPM6 = 1 end
	if rpm > (rz*p8) 	then mRPMLedTable.RPM7 = 1 end
	if rpm > (rz*p9)	then mRPMLedTable.RPM8 = 1 end

	if rpm > (rz*p10) 	then mRPMLedTable.RPM9 = 1 end
	if rpm > (rz*p11)	then mRPMLedTable.RPM10 = 1 end
	if rpm > (rz*p12)	then mRPMLedTable.RPM11 = 1 end
	if rpm > (rz*p13)	then mRPMLedTable.RPM12 = 1 end
end
	
-- alternate green, red and blue method
function AlternateSLI(rpm, redzone, step1, step2, step3)

	local rz = redzone / 13
	if rpm > (rz*step1)  then
		mRPMLedTable.RPM0 = 1	 -- G
		mRPMLedTable.RPM1 = 1	 -- G
		mRPMLedTable.RPM2 = 1	 -- G
		mRPMLedTable.RPM3 = 1	 -- G
	end
	if rpm > (rz*step2) then
		mRPMLedTable.RPM4 = 1	 -- R
		mRPMLedTable.RPM5 = 1	 -- R
		mRPMLedTable.RPM6 = 1	 -- R
		mRPMLedTable.RPM7 = 1	 -- R
		mRPMLedTable.RPM8 = 1	 -- R
	end
	if rpm > (rz*step3)  then
		mRPMLedTable.RPM9 = 1	 -- B
		mRPMLedTable.RPM10 = 1	 -- B
		mRPMLedTable.RPM11 = 1	 -- B
		mRPMLedTable.RPM12 = 1	 -- B
	end
end

function PercentageSLI(rpm, redzone, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 )
	local onePercent = redzone / 100
	if p1>0 and rpm >= (p1*onePercent) then mRPMLedTable.RPM0 = 1 end -- G
	if p2>0 and rpm >= (p2*onePercent) then mRPMLedTable.RPM1 = 1	 end -- G
	if p3>0 and rpm >= (p3*onePercent) then mRPMLedTable.RPM2 = 1	 end -- G
	if p4>0 and rpm >= (p4*onePercent) then mRPMLedTable.RPM3 = 1	 end -- G
	if p5>0 and rpm >= (p5*onePercent) then mRPMLedTable.RPM4 = 1	 end -- R
	if p6>0 and rpm >= (p6*onePercent) then mRPMLedTable.RPM5 = 1	 end -- R
	if p7>0 and rpm >= (p7*onePercent) then mRPMLedTable.RPM6 = 1	 end -- R
	if p8>0 and rpm >= (p8*onePercent) then mRPMLedTable.RPM7 = 1	 end -- R
	if p9>0 and rpm >= (p9*onePercent) then mRPMLedTable.RPM8 = 1	 end -- R
	if p10>0 and rpm >= (p10*onePercent) then mRPMLedTable.RPM9 = 1	 end -- B
	if p11>0 and rpm >= (p11*onePercent) then mRPMLedTable.RPM10 = 1	 end -- B
	if p12>0 and rpm >= (p12*onePercent) then mRPMLedTable.RPM11 = 1	 end -- B
	if p13>0 and rpm >= (p13*onePercent) then mRPMLedTable.RPM12 = 1	 end -- B
	-- print("RPM:".. rpm .. " Percentage:" .. onePercent .. " RZ:" .. gRedZone .. " P13:" .. mRPMLedTable.RPM12.. "\n")
end

-- fixed rpm method, set rpm for each led (NOT RECOMMENDED)
function RpmSLI(rpm, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 )
	if p1>0 and rpm >= p1 then mRPMLedTable.RPM0 = 1	 end -- G
	if p2>0 and rpm >= p2 then mRPMLedTable.RPM1 = 1	 end -- G
	if p3>0 and rpm >= p3 then mRPMLedTable.RPM2 = 1	 end -- Y
	if p4>0 and rpm >= p4 then mRPMLedTable.RPM3 = 1	 end -- Y
	if p5>0 and rpm >= p5 then mRPMLedTable.RPM4 = 1	 end -- R
	if p6>0 and rpm >= p6 then mRPMLedTable.RPM5 = 1	 end -- R
	if p7>0 and rpm >= p7 then mRPMLedTable.RPM6 = 1	 end -- G
	if p8>0 and rpm >= p8 then mRPMLedTable.RPM7 = 1	 end -- G
	if p9>0 and rpm >= p9 then mRPMLedTable.RPM8 = 1	 end -- Y
	if p10>0 and rpm >= p10 then mRPMLedTable.RPM9 = 1	 end -- Y
	if p11>0 and rpm >= p11 then mRPMLedTable.RPM10 = 1	 end -- R
	if p12>0 and rpm >= p12 then mRPMLedTable.RPM11 = 1	 end -- R
	if p13>0 and rpm >= p13 then mRPMLedTable.RPM12 = 1	 end -- R
end

-- progressive method but with rpm value for eah led
function ProgressiveFixedSLI(rpm, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13)
	-- progressive method with thresholds as parameters
	if rpm > p1 	then mRPMLedTable.RPM0 = 1 end
	if rpm > p2 	then mRPMLedTable.RPM1 = 1 end
	if rpm > p3		then mRPMLedTable.RPM2 = 1 end
	if rpm > p4 	then mRPMLedTable.RPM3 = 1 end

	if rpm > p5 	then mRPMLedTable.RPM4 = 1 end
	if rpm > p6  	then mRPMLedTable.RPM5 = 1 end
	if rpm > p7		then mRPMLedTable.RPM6 = 1 end
	if rpm > p8 	then mRPMLedTable.RPM7 = 1 end
	if rpm > p9		then mRPMLedTable.RPM8 = 1 end

	if rpm > p10 	then mRPMLedTable.RPM9 = 1 end
	if rpm > p11	then mRPMLedTable.RPM10 = 1 end
	if rpm > p12	then mRPMLedTable.RPM11 = 1 end
	if rpm > p13	then mRPMLedTable.RPM12 = 1 end
end
-- --------------------------------------------
-- BU0710 functions

-- progressive method for BU0710
function ProgressiveBU0710(rpm, redzone, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 )
	local rz = redzone / 10.0
	if rpm > (rz * p1) then mBU0710Leds = 0x1 end -- G
	if rpm > (rz * p2) then mBU0710Leds = 0x2 end -- G
	if rpm > (rz * p3) then mBU0710Leds = 0x3 end -- G
	if rpm > (rz * p4) then mBU0710Leds = 0x4 end -- Y
	if rpm > (rz * p5) then mBU0710Leds = 0x5 end -- Y
	if rpm > (rz * p6) then mBU0710Leds = 0x6 end -- Y
	if rpm > (rz * p7) then mBU0710Leds = 0x7 end -- R
	if rpm > (rz * p8) then mBU0710Leds = 0x8 end -- R
	if rpm > (rz * p9) then mBU0710Leds = 0x9 end -- R
	if rpm > (rz * p10) then mBU0710Leds = 0xA end -- R
	if rpm > (rz * p11) then mBU0710Leds = 0xB end -- RRRR
end

-- percentage method for BU0710
function PercentageBU0710(rpm, redzone, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 )
	local onePercent = redzone / 100
	if rpm > (p1 * onePercent) then mBU0710Leds = 0x1 end -- G
	if rpm > (p2 * onePercent) then mBU0710Leds = 0x2 end -- G
	if rpm > (p3 * onePercent) then mBU0710Leds = 0x3 end -- G
	if rpm > (p4 * onePercent) then mBU0710Leds = 0x4 end -- Y
	if rpm > (p5 * onePercent) then mBU0710Leds = 0x5 end -- Y
	if rpm > (p6 * onePercent) then mBU0710Leds = 0x6 end -- Y
	if rpm > (p7 * onePercent) then mBU0710Leds = 0x7 end -- R
	if rpm > (p8 * onePercent) then mBU0710Leds = 0x8 end -- R
	if rpm > (p9 * onePercent) then mBU0710Leds = 0x9 end -- R
	if rpm > (p10 * onePercent) then mBU0710Leds = 0xA end -- R
	if rpm > (p11 * onePercent) then mBU0710Leds = 0xB end -- RRRR
end

-- fixed rpm method for BU0710 (NOT RECOMMENDED)
function RpmBU0710(rpm, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 )
	if rpm > p1 then mBU0710Leds = 0x1 end -- G
	if rpm > p2 then mBU0710Leds = 0x2 end -- G
	if rpm > p3 then mBU0710Leds = 0x3 end -- G
	if rpm > p4 then mBU0710Leds = 0x4 end -- Y
	if rpm > p5 then mBU0710Leds = 0x5 end -- Y
	if rpm > p6 then mBU0710Leds = 0x6 end -- Y
	if rpm > p7 then mBU0710Leds = 0x7 end -- R
	if rpm > p8 then mBU0710Leds = 0x8 end -- R
	if rpm > p9 then mBU0710Leds = 0x9 end -- R
	if rpm > p10 then mBU0710Leds = 0xA end -- R
	if rpm > p11 then mBU0710Leds = 0xB end -- RRRR
end
		
function GetKersPercent()
	-- get kers value
	local kers_level = GetCarInfo("kers")
	local k_percent = 0
	if kers_level ~= nil and kers_level > 0 then
		k_percent = round((kers_level/1000) / 4 )
	end
	return k_percent
end
-- ==============================================================================

 -- SLI-M, SLI-PRO ShiftLights Methods Event
function shiftLightsMethodEvent(idx)
	-- get red zone (red zone default value is initialized in gear.lua )
	if gRedZone == nil or gRedZone == 0 then gRedZone = GetCarInfo("redzone") end

	-- call custom script
	local result = custom_shiftLightsMethodEvent(idx)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- call global custom script
	result = global_custom_shiftLightsMethodEvent(idx)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue

	-- get rpm
	local rpm = 0
	rpm = GetCarInfo("rpm")
	if rpm == nil then rpm = 0 end
	
	-- init leds (see global.lua)
	initLedTable(mRPMLedTable, 0)
	
	if idx == 0 then
		-- progressive method 0
		ProgressiveSLI(rpm, gRedZone, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 12.8, 12.98 )
		
	elseif idx == 1 then
		-- alternate method 1
		AlternateSLI(rpm, gRedZone, 11.0, 12.0, 12.9)
		
	elseif idx == 2 then
		-- percentage method 2
		PercentageSLI(rpm, gRedZone, RPM_PERCENT_VALUES[1], RPM_PERCENT_VALUES[2], RPM_PERCENT_VALUES[3], RPM_PERCENT_VALUES[4], RPM_PERCENT_VALUES[5], RPM_PERCENT_VALUES[6], RPM_PERCENT_VALUES[7], RPM_PERCENT_VALUES[8], RPM_PERCENT_VALUES[9], RPM_PERCENT_VALUES[10], RPM_PERCENT_VALUES[11], RPM_PERCENT_VALUES[12], RPM_PERCENT_VALUES[13] )
		
    elseif idx == 3 then
		-- abs rpm method 3 (not recommended) 
		RpmSLI(rpm, RPM_ABSOLUTE_VALUES[1], RPM_ABSOLUTE_VALUES[2], RPM_ABSOLUTE_VALUES[3], RPM_ABSOLUTE_VALUES[4], RPM_ABSOLUTE_VALUES[5], RPM_ABSOLUTE_VALUES[6], RPM_ABSOLUTE_VALUES[7], RPM_ABSOLUTE_VALUES[8], RPM_ABSOLUTE_VALUES[9], RPM_ABSOLUTE_VALUES[10], RPM_ABSOLUTE_VALUES[11], RPM_ABSOLUTE_VALUES[12], RPM_ABSOLUTE_VALUES[13] )
		
	elseif idx == 4 then
		-- side to center method 4
		SideToCenterSLI(rpm, gRedZone, 94.5, 95, 96, 97 ,98 ,99 ,99.5 )
		
	elseif idx == 5 then
		-- KERS + RPM
		
		-- get kers value
		local k_percent = GetKersPercent()
		
		-- without first 4 green leds
		AlternateSLI(rpm, gRedZone, 100, 11.6, 12.9)
	
		-- KERS feedback on green leds
		-- print("KERS: " .. kers_level .. " - " .. k_percent .. "\n")
	
		if k_percent > 0   then mRPMLedTable.RPM0 = 1 end
		if k_percent >= 25 then mRPMLedTable.RPM1 = 1 end
		if k_percent >= 50 then mRPMLedTable.RPM2 = 1 end
		if k_percent >= 75 then mRPMLedTable.RPM3 = 1 end
		
	elseif idx == 6 then
		-- Revers KERS + RPM
		
		-- get kers value
		local k_percent = GetKersPercent()
	
		-- without first 4 green leds
		AlternateSLI(rpm, gRedZone, 0, 11.6, 12.9)
	
		-- KERS feedback on green leds
		-- print("KERS: " .. kers_level .. " - " .. k_percent .. "\n")
	
		if k_percent > 0   then mRPMLedTable.RPM3 = 0 end
		if k_percent >= 25 then mRPMLedTable.RPM2 = 0 end
		if k_percent >= 50 then mRPMLedTable.RPM1 = 0 end
		if k_percent >= 75 then mRPMLedTable.RPM0 = 0 end
		
   else
        return 0
	end

	local tName = "mRPMLedTable"
	SetRPMLed(tName)
	return 1
end

-- BU0710 ShiftLights Methods Event
function shiftLightsBU0710Event(idx)

	-- get red zone (red zone default value is initialized in gear.lua )
	if gRedZone == nil or gRedZone == 0 then gRedZone = GetCarInfo("redzone") end

	-- call custom script
	local result = custom_shiftLightsBU0710Event(idx)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue
	
	-- call global custom script
	result = global_custom_shiftLightsBU0710Event(idx)
	-- if result = 0 bypass the script below and return 0
	-- if result = 1 bypass the script below and return 1
	if result <= 1 then return result end
	-- if result >= 2 continue

	local rpm = GetCarInfo("rpm")
	if rpm == nil then rpm = 0 end
	
	-- init leds (see global.lua)
	mBU0710Leds = 0
	
	if (idx <= 1) or (idx >= 4) then
		-- progressive methods 1 or 4
		ProgressiveBU0710(rpm, gRedZone, 4.0, 6.0, 7.0, 7.5, 8.0, 8.5, 9.0, 9.3, 9.5, 9.7, 9.9 )

	elseif (idx == 2) then
		-- percentage method 2
		PercentageBU0710(rpm, gRedZone, 84, 88, 90, 92, 93, 94, 95, 96, 97, 98, 99 )

	elseif (idx == 3) then
		-- abs method 3 (not recommended)
		RpmBU0710(rpm, 15823, 16354, 16410, 16675, 17252, 17545, 17823, 18354, 18510, 18655, 18675 )

	else
		return 0
	end
	SetBU0710Led(mBU0710Leds)
	return 1
end